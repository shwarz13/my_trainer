import React, { Fragment, useEffect, useState } from 'react';
import { hot } from 'react-hot-loader';
import { connect } from 'react-redux';

import Input from 'Components/baseComponents/Input';
import Avatar from 'Components/baseComponents/Avatar';

// styles
import './index.scss';
import { setLeftBtn, setRightBtn } from 'Store/header/actions';
import { IHeaderActions } from 'Store/header/i';
import { showSpinner } from 'Store/interface/actions';
import { IMainInterfaceActions } from 'Store/interface/i';
import { Link, Route, Switch } from 'react-router-dom';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { ExpansionPanel, ExpansionPanelDetails, ExpansionPanelSummary, Typography } from '@material-ui/core';
import loadable from 'loadable-components';
import { updateUserInfo } from 'Store/user_info/actions';
import { IUserInfoActions } from 'Store/user_info/i';
import { clientLogout } from 'Store/auth/actions';
import { IAuthActions } from 'Store/auth/i';
import { makeStyles } from '@material-ui/core/styles';
import ChangePasswordComponent from './changePassword';

type IProps = {
	userInfo?: any;
	location?: any;
	history?: any;
} & IHeaderActions & IMainInterfaceActions & IUserInfoActions & IAuthActions;

const path = require('Constants').initialPath;

const useStyles = makeStyles(theme => ({
	root: {
		width: '100%',
	},
	heading: {
		fontSize: theme.typography.pxToRem(15),
		fontWeight: theme.typography.fontWeightRegular,
	},
}));

const Profile = (props: IProps) => {

	const [email, setEmail] = useState(props.userInfo.email);
	const [firstName, setFirstName] = useState(props.userInfo.firstName);
	const [lastName, setLastName] = useState(props.userInfo.lastName);
	const [aboutMe, setAboutMe] = useState('');
	const [avatar, setAvatar] = useState(props.userInfo.avatar);
	const [isEdit, setIsEdit] = useState(false);

	const classes = useStyles();

	let prevState = {
		email, firstName, lastName
	};

	const backInitialState = () => {
		setEmail(prevState.email);
		setFirstName(prevState.firstName);
		setLastName(prevState.lastName);
	};

	useEffect(() => {
		prevState = {email, firstName, lastName};
	}, []);

	useEffect(() => {

		if (props.location.pathname === `${path}lk/profile`) {
			if (isEdit) {
				props.setLeftBtn({isShow: true, text: 'Отменить', func: () => {
					backInitialState();
					props.setLeftBtn({isShow: true, text: 'Править', func: () => setIsEdit(true)});
					props.setRightBtn({isShow: false, text: '', func: null});
					setIsEdit(false);
				}});
				props.setRightBtn({isShow: true, text: 'Сохранить', func: async () => {
						await props.showSpinner({show: true});
						try {
							await props.updateUserInfo({email, firstName, lastName});
							prevState = {email, firstName, lastName};
							props.setLeftBtn({isShow: true, text: 'Править', func: () => setIsEdit(true)});
							props.setRightBtn({isShow: false, text: '', func: null});
							props.showSpinner({show: false});
						} catch (e) {
							alert('ERRROOOOOOR!!!!!');
							backInitialState();
						}
					}
				});
			} else {
				props.setLeftBtn({isShow: true, text: 'Править', func: () => setIsEdit(true)});
			}
		}

		return () => {
			props.setLeftBtn({isShow: false, text: '', func: null});
			props.setRightBtn({isShow: false, text: '', func: null});
		};
	}, [isEdit, email, firstName, lastName]);

	const routes = [
		{
			title: 'Статистика',
			path: `${path}lk/profile/statistics`,
			component: loadable(() => import('./statistics'))
		},
		{
			title: 'Сообщения',
			path: `${path}lk/profile/messages`,
			notification: 6,
			component: loadable(() => import('./messages'))
		},
		{
			title: 'Показатели',
			path: `${path}lk/profile/achievement`,
			component: loadable(() => import('./achievement'))
		},
		{
			title: 'Тренер',
			path: `${path}lk/profile/trainer`,
			component: loadable(() => import('./trainer'))
		}
	];

	return (
		<div className="content content-user-info">
			<Switch>
				<Route
					exact={true}
					path={`${path}lk/profile`}
					render={() =>
						<Fragment>
							<div className="content-user-info__user-info flex-direction_row">
								<Avatar alt={`${firstName[0]}`} src={avatar} size={'large'}/>
								<div className="content-user-info__user-info__full-name">
									<Input value={firstName} placeholder={'Имя'} onChange={setFirstName} disabled={!isEdit}/>
									<Input value={lastName} placeholder={'Фамилия'} onChange={setLastName} disabled={!isEdit}/>
								</div>
							</div>
							{
								isEdit ?
									<Fragment>
										<div className="content-user-info__user-info content-user-info__user-info_edit">
											<Input value={aboutMe} placeholder={'О себе'} onChange={value => setAboutMe(value)} multiline={true} rowsMax={4}/>
											<Input value={'8(906)3534677'} placeholder={'Телефон'} onChange={value => console.log(value)} />
											<Input value={email} onChange={value => setEmail(value)} placeholder={'Email'}/>
										</div>
										<div className="content-user-info__user-info__change-password">
											<div className="content-user-info__user-info__change-password__item" onClick={() => props.history.push(`${path}lk/profile/changePassword`)}>
												<span className={'content-user-info__user-info__change-password__item__title'}>Изменить пароль</span>
												<ChevronRightIcon/>
											</div>
										</div>
									</Fragment> :
									<Fragment>
										<div className="content-user-info__user-info">
											{
												routes.map( ({title, path, notification=''}) =>
													<Link className="content-user-info__user-info__link" key={title} to={path}>
														{title}
														<span className={'content-user-info__user-info__link__notifications'}>
														{
															notification ?
																<span className={'content-user-info__user-info__link__notifications__value'}>{notification}</span>:
																null
														}
															<ChevronRightIcon/>
														</span>
													</Link>
												)
											}
										</div>
										<div className="btn-logout" onClick={()=>props.clientLogout({})}>Выйти</div>
									</Fragment>
							}
						</Fragment>
					}
				/>
				{
					routes.map( ({ path, component: C, title }) =>
						<Route
							key={`route-${title}`}
							render={ props => <C {...props}/>}
							{...{path}}
						/>
					)
				}
				<Route
					exact={true}
					path={`${path}lk/profile/changePassword`}
					component={() => <ChangePasswordComponent history={props.history}/>}
				/>
			</Switch>
		</div>
	);
};

const mapStateToProps = (state) => {
	return  {
		userInfo: state.userInfo.userInfo,
		loaded: state.loaded.GET_USER_INFO_SUCCESS
	}
};

export default hot(module)(
	connect(
		mapStateToProps,
		{ setLeftBtn, setRightBtn, showSpinner, updateUserInfo, clientLogout }
	)(Profile)
);
