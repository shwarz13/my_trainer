import logger from "../../utils/Logger";

import { Op, Transaction } from "sequelize";
import { INewTask } from "./interfaces";
import { server } from "../../server";

import SetMethods from "../Set";
import SystemError from "../../utils/SystemError";

class Methods {
  public static async read() {
    return [];
  }

  public static async create(
    tasks: INewTask[],
    incomingTransaction?: Transaction
  ) {
    const transaction =
      incomingTransaction || (await server.dbConnect.transaction());
    try {
      const result = await Promise.all(
        tasks.map(async ({ sets, ...task }) => {
          const taskInstance = await server.Task.create(task, { transaction });
          await SetMethods.create(
            sets.map(set => ({ ...set, task: taskInstance.id })),
            transaction
          );
        })
      );
      if (!incomingTransaction) {
        await transaction.commit();
      }
    } catch (error) {
      if (!incomingTransaction) {
        await transaction.rollback();
      }
      if (error instanceof SystemError) {
        throw error;
      }
      throw error;
    }
  }
}

export default Methods;
