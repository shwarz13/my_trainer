import { AnyAction } from 'redux';
import { IUserInfoState, ActionTypes as AT } from './i';
import { RequestState } from 'Constants';


const initialState: IUserInfoState = {
  userInfo: {},
  changePasswordSuccess: false,
  changePasswordFail: false
};

export default (state = initialState, action: AnyAction) => {
  switch (action.type) {
    case AT.GET_USER_INFO + RequestState.SUCCESS: {
      return {
        ...state,
        userInfo: { ... action.payload }
      };
    }
    case AT.UPDATE_USER_INFO + RequestState.SUCCESS: {
      const {firstName, lastName, phone, email} = action.other;
      return {
        ...state,
        userInfo: { firstName, lastName, phone, email }
      };
    }
    case AT.CHANGE_PASSWORD + RequestState.SUCCESS: {
      return {
        ...state,
        changePasswordSuccess: true,
        changePasswordFail: false
      }
    }
    case AT.CHANGE_PASSWORD + RequestState.FAIL: {
      return {
        ...state,
        changePasswordSuccess: false,
        changePasswordFail: true
      }
    }
    case AT.RESET_PASSWORD: {
      return {
        ...state,
        changePasswordSuccess: false,
        changePasswordFail: false
      }
    }
    default: {
      return state;
    }
  }
};
