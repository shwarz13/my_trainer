import Sequelize from "sequelize";
import { BASE_INSTANCE_FIELDS } from "../../../../database/constants";

const schema = {
  ...BASE_INSTANCE_FIELDS,
  exercise: {
    type: Sequelize.UUID,
    allowNull: false
  },
  parameter: {
    type: Sequelize.UUID,
    allowNull: false
  },
  required: {
    type: Sequelize.BOOLEAN,
    allowNull: false
  }
};

export default schema;
