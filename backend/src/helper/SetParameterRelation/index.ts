import logger from "../../utils/Logger";

import trycatcher from "../../utils/trycatcher";

import { Op, Transaction } from "sequelize";
import { server } from "../../server";
import { ISetParameterRelationBase } from "./interfaces";
import SystemError from "../../utils/SystemError";
import { HTTP_ERROR_CODES } from "../../constants";
import { ErrorMessages } from "./constants";

class Methods {
  public static async read() {
    return [];
  }

  public static async create(
    setParameters: ISetParameterRelationBase[],
    incomingTransaction?: Transaction
  ) {
    const transaction =
      incomingTransaction || (await server.dbConnect.transaction());
    try {
      const result = await server.SetPerameterRelation.bulkCreate(
        setParameters,
        { transaction }
      );
      if (!incomingTransaction) {
        await transaction.commit();
      }
      return result;
    } catch (error) {
      if (!incomingTransaction) {
        await transaction.rollback();
      }
      if (error instanceof SystemError) {
        throw error;
      }
      throw error;
    }
  }
}

export default Methods;
