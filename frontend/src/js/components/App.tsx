import React, { Component } from 'react';
import { connect } from 'react-redux';
import { hot } from 'react-hot-loader';
import { withRouter, Route, Switch, Redirect } from 'react-router-dom';

// components
import MainPage from './cabinet';
import Auth from './signIn';
import Registration from './signUp';

// store
import { setSession } from 'Store/auth/actions';
import { IAuthActions } from 'Store/auth/i';
import { getUserInfo } from 'Store/user_info/actions';
import { IUserInfoActions } from 'Store/user_info/i';
// import { showMessage, showModal } from 'Store/interface/actions';

// libs
import Cookies from 'universal-cookie';

// styles
import 'Styles/main.scss';

type IProps = {
  location: any;
  loggedIn?: boolean;
  match: {
    url: string
  };
  userInfo: any;
  message: any;
  modalWindow: any;
} & IAuthActions & IUserInfoActions;

// tslint:disable-next-line:no-var-requires
const path = require('Constants').initialPath;

class App extends Component<IProps> {

  public constructor(props) {
    super(props);
  }

  public state = {
    bgIndex: Math.floor(Math.random() * 7) + 1
  };

  public componentDidMount() {
    const cookies = new Cookies();
    const token = cookies.get('token');
    const { loggedIn } = this.props;

    if (typeof token !== 'undefined' && loggedIn) {
      // tslint:disable-next-line:no-unused-expression
      token && this.props.setSession({ token });
    }
  }

  public render() {
    const { loggedIn } = this.props;
    return (
      <>
        <Switch>
          <Route
            exact
            path={`${path}`}
            component={() => <Redirect exact from={`${path}`} to={`${path}lk`} />}
          />
          <Route
            path={`${path}lk`}
            render={props => {
              return loggedIn ? <MainPage {...props} /> : <Redirect exact from={`${path}`} to={`${path}sign_in`} />;
            }}
          />
          <Route
            path={`${path}sign_in`}
            render={props => <React.Fragment><div className={`entrance_bg_${this.state.bgIndex}`}/><Auth {...props}/></React.Fragment>}
          />
          <Route
            path={`${path}sign_up`}
            render={props => <React.Fragment><div className={`entrance_bg_${this.state.bgIndex}`}/><Registration {...props}/></React.Fragment>}
          />
        </Switch>
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    loggedIn: state.auth.loggedIn,
    message: state.mainInterface.showMessage,
    modalWindow: state.mainInterface.showModal
  };
};

export default hot(module)(
  withRouter(
    connect(
      mapStateToProps,
      { setSession, getUserInfo }
    )(App) as any
  )
);








