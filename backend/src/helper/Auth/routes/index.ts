import {
  MAIN_URI,
  HTTP_METHODS,
  MIN_PASSWORD_LENGTH
} from "../../../constants/index";
import Boom from "@hapi/boom";
// interfaces
import { ServerRoute } from "@hapi/hapi";
// controllers
import authCtrl from "../controllers";

import {
  login,
  refresh,
  registration,
  resetForgottenPassword,
  resetForgottenPasswordComplete,
  resetPassword
} from "../docs/";
import { routeName, pathParams } from "../constants";
import Joi from "@hapi/joi";

const Routes: ServerRoute[] = [
  {
    method: HTTP_METHODS.POST,
    path: `${MAIN_URI}/${routeName}/${pathParams.REGISTRATION}`,
    handler: authCtrl.registration,
    options: {
      ...registration,
      auth: false,
      validate: {
        payload: Joi.object({
          firstName: Joi.string()
            .required()
            .min(2),
          lastName: Joi.string()
            .required()
            .min(2),
          phone: Joi.string()
            .optional()
            .description("Номер телефона указывать без +7 и без 8")
            .allow(null),
          email: Joi.string()
            .required()
            .email(),
          password: Joi.string()
            .required()
            .min(8)
        })
      }
    }
  },
  {
    method: HTTP_METHODS.POST,
    path: `${MAIN_URI}/${routeName}/${pathParams.LOGIN}`,
    handler: authCtrl.login,
    options: {
      ...login,
      auth: false,
      validate: {
        payload: Joi.object({
          login: Joi.string()
            .required()
            .email(),
          password: Joi.string()
            .required()
            .min(MIN_PASSWORD_LENGTH)
        })
      }
    }
  },
  {
    method: HTTP_METHODS.POST,
    path: `${MAIN_URI}/${routeName}/${pathParams.REFRESH}`,
    handler: authCtrl.refresh,
    options: {
      ...refresh,
      auth: false,
      validate: {
        payload: Joi.object({
          token: Joi.string().required(),
          refreshToken: Joi.string().required()
        })
      }
    }
  },
  {
    method: HTTP_METHODS.POST,
    path: `${MAIN_URI}/${routeName}/${pathParams.RESET_PASS}`,
    handler: authCtrl.resetPassword,
    options: {
      ...resetPassword,
      validate: {
        payload: Joi.object({
          oldPassword: Joi.string()
            .required()
            .min(8),
          newPassword: Joi.string()
            .required()
            .min(8)
        })
      }
    }
  },
  {
    method: HTTP_METHODS.GET,
    path: `${MAIN_URI}/${routeName}/${pathParams.RESET_PASS}/${pathParams.FORGOTTEN}/{login}`,
    handler: authCtrl.resetForgottenPassword,
    options: {
      ...resetForgottenPassword,
      auth: false,
      validate: {
        params: Joi.object({
          login: Joi.string()
            .required()
            .email()
        })
      }
    }
  },
  {
    method: HTTP_METHODS.POST,
    path: `${MAIN_URI}/${routeName}/${pathParams.RESET_PASS}/${pathParams.FORGOTTEN}/${pathParams.COMPLETE}`,
    handler: authCtrl.resetForgottenPasswordComplete,
    options: {
      ...resetForgottenPasswordComplete,
      auth: "reset-pass-jwt",
      validate: {
        payload: Joi.object({
          newPassword: Joi.string()
            .required()
            .min(8)
        })
      }
    }
  }
];

export default Routes;
