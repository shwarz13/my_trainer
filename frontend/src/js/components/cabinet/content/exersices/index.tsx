import React, { Fragment, useEffect } from 'react';
import { hot } from 'react-hot-loader';
import { connect } from 'react-redux';
import ExersiceItem from './exerciseItem';
import './index.scss';
import { IExercisesActions } from 'Store/exercise/i';
import { setRightBtn } from 'Store/header/actions';
import { IHeaderActions } from 'Store/header/i';
import AddIcon from '@material-ui/icons/Add';
import { Input } from '@material-ui/core';

type IProps = {
	exercises?: any;
	images?: any;
	location?: any;
} & IExercisesActions & IHeaderActions;

// tslint:disable-next-line:no-var-requires
const path = require('Constants').initialPath;

const Exersices = (props: IProps) => {

	useEffect( () => {
		if (props.location.pathname === `${path}lk/exercises`) {
			props.setRightBtn({isShow: true, component: <AddIcon fontSize="large"/>, func: async () => console.log('link')	});
		}
		return () => props.setRightBtn({isShow: false, component: null, func: null});
	}, [] );

	const exercises = [
		{id:'00a1', name: 'Жир лежа', img: 'https://news.artnet.com/app/news-upload/2020/01/baldessari-systems-still-001-013-256x256.jpg', description: '', createdAt: '15.05.2019'},
		{id:'00a2', name: 'Двойной полуприсед', img: 'https://pbs.twimg.com/profile_images/419933853185089536/CykEyWKM.png', description: '', createdAt: '17.05.2019'},
		{id:'00a3', name: 'Подъем ног в висе', img: 'https://secure.gravatar.com/avatar/1eeac303d38f1197feb37cdef888f9d5?s=256&d=mm&r=r', description: '', createdAt: '25.05.2009'},
		{id:'00a4', name: 'Подьем штанги на бицепс стоя', img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRtjXAbOoP6L83_kyfuJCDhd1PnGeii3jKZaMPXXnBEQa19H2xS', description: '', createdAt: '09.11.1999'},
		{id:'00a11', name: 'Жир лежа на наклонной скамье', img: 'https://news.artnet.com/app/news-upload/2020/01/baldessari-systems-still-001-013-256x256.jpg', description: '', createdAt: '15.05.2019'},
		{id:'00a12', name: 'Четверной полуприсед', img: 'https://pbs.twimg.com/profile_images/419933853185089536/CykEyWKM.png', description: '', createdAt: '17.05.2019'},
		{id:'00a13', name: 'Подъем ног в висячем положении', img: 'https://secure.gravatar.com/avatar/1eeac303d38f1197feb37cdef888f9d5?s=256&d=mm&r=r', description: '', createdAt: '25.05.2009'},
		{id:'00a41', name: 'Подьем гантели на бицепс сидя', img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRtjXAbOoP6L83_kyfuJCDhd1PnGeii3jKZaMPXXnBEQa19H2xS', description: '', createdAt: '09.11.1999'},
	];

	return (
		<div className={'content content-exercises'}>
			<div className={'content-exercises__search search'}>
				<Input type={'search'} placeholder={'Поиск упражнения'} />
			</div>
			<div className={'content-exercises__list'}>
				{
					exercises ?
						exercises.map( ({id, name, createdAt, img}) =>
							<ExersiceItem key={id} {...{id, name, createdAt, img}}/>
						) :
						null
				}
			</div>
		</div>
	);
};

const mapStateToProps = (state) => {
	return  {
		exercises: state.exercises.exercises
	}
};


export default hot(module)(
	connect(
		mapStateToProps,
		{ setRightBtn }
		// @ts-ignore
	)(Exersices)
);
