import { server } from "../../server";

import {
  IUserInstance,
  ICreateUserPayload,
  IUpdateUserPayload
} from "./interfaces";
import { attrFind } from "./constants";

export default class User {
  public static async findByLoginPassword(
    login: string,
    password: string
  ): Promise<IUserInstance | null> {
    const userInstance = server.User.findOne({
      where: {
        email: login
      },
      attributes: ["id"]
    });
    // @ts-ignore
    if (!userInstance || (await userInstance.validPassword(password))) {
      return null;
    }
    return userInstance;
  }

  public static async findUserById(
    id,
    attributes = attrFind.default
  ): Promise<IUserInstance | null> {
    return server.User.findByPk(id, {
      attributes
    });
  }

  public static async findByLogin(
    login,
    attributes = ["id"]
  ): Promise<IUserInstance | null> {
    return server.User.findOne({
      where: {
        email: login
      },
      attributes
    });
  }

  public static async create(userData: ICreateUserPayload) {
    return server.User.create(userData, { returning: true });
  }

  public static async update(userId: string, newData: IUpdateUserPayload) {
    const dataForUpdate: any = { ...newData };

    return server.User.update(dataForUpdate, {
      where: {
        id: userId
      },
      returning: true
    });
  }
}
