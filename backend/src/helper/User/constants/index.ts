import { HTTP_ERROR_CODES } from "../../../constants";

export const routeName = "user";

export const attrFind = {
  default: [
    "id",
    "email",
    "phone",
    "firstName",
    "lastName",
    "avatar",
    "socials"
  ],
  mobile: []
};

export enum pathParams {
  ABOUT = "about"
}

export enum ErrorMessages {}
