const path = require('path');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const dotenvWebpack = require('dotenv-webpack');
const copyPlugin = require('copy-webpack-plugin');

module.exports = (env, argv) => {
  const mode = typeof env !== 'undefined' ? env : argv.mode;
  const isDev = mode === 'development';
  console.log(`mode: ${mode}, isDev: ${isDev}`);

  const { readFileSync } = require('fs');
  const version = readFileSync('./version.num')
    .toString()
    .replace(/\s/, '');

  const plugins = [
    new HtmlWebPackPlugin({
      filename: 'index.html',
      template: './src/index.html'
    }),
    new MiniCssExtractPlugin({
      filename: '[name].[contenthash].css',
      chunkFilename: '[name].[contenthash].css',
    }),
    new copyPlugin([{
      from: './src/static'
    }]),
    // new BundleAnalyzerPlugin()
  ];

  if (isDev) {
    plugins.push(new dotenvWebpack());
  }

  return {
    entry: './src/index.tsx',
    resolve: {
      extensions: ['.ts', '.tsx', '.js', '.jsx', 'json'],
      alias: {
        Constants: path.resolve(__dirname, './src/js/constants.ts'),
        Exports: path.resolve(__dirname, './src/js/exports.ts'),
        Styles: path.resolve(__dirname, './src/styles'),
        Store: path.resolve(__dirname, './src/js/store'),
        Components: path.resolve(__dirname, './src/js/components'),
        Containers: path.resolve(__dirname, './src/js/containers'),
        Utils: path.resolve(__dirname, './src/js/utils'),
        Images: path.resolve(__dirname, './src/img')
      }
    },
    output: {
      path: path.join(__dirname, '/dist'),
      filename: `bundle.min.[hash].js`,
      publicPath: '/'
    },
    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          exclude: /node_modules/,
          use: {
            loader: 'babel-loader'
          }
        },
        {
          test: /\.html$/,
          use: [
            {
              loader: 'html-loader'
            }
          ]
        },
        {
          test: /\.(ts|tsx)?$/,
          include: path.resolve(__dirname, 'src'),
          use: [
            {
              loader: 'awesome-typescript-loader'
            }
          ]
        },
        {
          test: /\.(sa|sc|c)ss$/,
          use: [
            isDev ? 'style-loader' : MiniCssExtractPlugin.loader,
            'css-loader',
            {
              loader: 'postcss-loader',
              options: {
                config: {
                  path: `configs/postcss.config.js`
                }
              }
            },
            'sass-loader'
          ]
        },
        {
          test: /\.(woff(2)?|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
          use: [{
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: 'fonts/'
            }
          }]
        },
        {
          test: /\.(png|jpg|gif)$/,
          include: path.resolve(__dirname, './src/img'),
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[path][name].[ext]',
                outputPath: 'images/'
              }
            }
          ]
        },
        {
          test: /\.(ico)$/,
          include: path.resolve(__dirname, './src'),
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[path][name].[ext]',
                outputPath: 'icons/'
              }
            }
          ]
        },
        {
          test: /\.svg$/,
          use: [
            {
              loader: 'babel-loader'
            },
            {
              loader: 'react-svg-loader',
              options: {
                jsx: true // true outputs JSX tags
              }
            }
          ]
        },
        {
          include: [/node_modules\/nookies/],
          loader: "babel-loader",
          test: /\.+(js|jsx)$/
        }
      ]
    },
    watchOptions: {
      poll: true
    },
    devServer: {
      historyApiFallback: true,
      hot: true
    },
    plugins
  };
};
