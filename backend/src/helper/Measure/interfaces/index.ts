import { IBaseInstanceFields } from "../../../interfaces";
import Sequelize from "sequelize";

/* *
 *   Measure
 * */

export interface IMeasureBase {
  name: string;
  description?: string | null;
}

export interface IMeasure extends IMeasureBase, IBaseInstanceFields {}

export type IMeasureInstance = Sequelize.Instance<IMeasure> & IMeasure;

export interface IMeasureMeta extends IMeasureBase {
  id: string;
}
