// import { isSystemError } from "general/interfaces";
import logger from "../utils/Logger";
// import trycatcher from "../utils/trycatcher";
import jwt from "jsonwebtoken";
import uuid from "uuid";
import {
  ADMIN_SECRET_TOKEN,
  APP_STATUS,
  SECRET_TOKEN,
  RESETPASS_SECRET_TOKEN
} from "../constants/env";

const Auth = {
  // Валидация запросов для системных событий
  UserJWTAuth: async (request, userJwt: string, h) => {
    try {
      const secretKey = SECRET_TOKEN;

      const jwtDecoded = jwt.verify(userJwt, secretKey) as { token: string };

      return { isValid: true, credentials: jwtDecoded };
    } catch (error) {
      logger.debug("User JWTAuth --> ", error);
      return { isValid: false, credentials: {} };
    }
  },
  AdminJWTAuth: async (request, adminJwt: string, h) => {
    const secretKey = ADMIN_SECRET_TOKEN;

    try {
      const jwtDecoded = jwt.verify(adminJwt, secretKey) as { token: string };

      return { isValid: true, credentials: { ...jwtDecoded, admin: true } };
    } catch (error) {
      logger.debug("Admin JWTAuth --> ", secretKey, error);
      return { isValid: false, credentials: {} };
    }
  },
  ResetPassJWTAuth: async (request, resetJwt: string, h) => {
    try {
      const secretKey = RESETPASS_SECRET_TOKEN;

      const jwtDecoded = jwt.verify(resetJwt, secretKey) as { token: string };

      return { isValid: true, credentials: jwtDecoded };
    } catch (error) {
      logger.debug("Reset pass JWTAuth --> ", error);
      return { isValid: false, credentials: {} };
    }
  }
};

export default Auth;
