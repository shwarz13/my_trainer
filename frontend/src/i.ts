import { IAuthActions } from 'Store/auth/i';

export type IUsedActions = IAuthActions;

export interface IProps {
  store: any;
}
