import { requestCreator } from '../request-creator';
import {MAIN_URL} from 'Constants';

import {
  IGetExercises,
  ICreateExercise, IGetImages,
  ActionTypes as AT, IGetOneExercise, IUpdateExercise
} from './i';
import { RequestType } from 'Constants';

export const createExercise: ICreateExercise = ({name, type, inn, kpp, email, phone, website}) => {
  return dispatch =>
    requestCreator(dispatch, {
      type: AT.CREATE_EXERCISE,
      requestUrl: `${MAIN_URL}admin/Exercises`,
      requestType: RequestType.POST,
      sendObject: { name, type, inn, kpp, email, phone, website }
    });
};

export const getExercises: IGetExercises = ({}) => {
    return dispatch =>
        requestCreator(dispatch, {
        type: AT.GET_EXERCISES,
        requestUrl: `${MAIN_URL}exercise/?page=2`,
        requestType: RequestType.GET,
        });
};

export const getImages: IGetImages = ({}) => {
  return dispatch =>
    requestCreator(dispatch, {
      type: AT.GET_IMAGES,
      requestUrl: `${MAIN_URL}exerciseimage`,
      requestType: RequestType.GET,
    });
};

export const getOneExercise: IGetOneExercise = ({id}) => {
  return dispatch =>
    requestCreator(dispatch, {
      type: AT.GET_ONE_EXERCISE,
      requestUrl: `${MAIN_URL}exercise/${id}`,
      requestType: RequestType.GET
    });
};

export const updateExercise: IUpdateExercise = ({id ,name, type, inn, kpp, email, phone, website}) => {
  return dispatch =>
    requestCreator(dispatch, {
      type: AT.UPDATE_EXERCISE,
      requestUrl: `${MAIN_URL}admin/Exercises/${id}`,
      requestType: RequestType.PUT,
      sendObject: { name, type, inn, kpp, email, phone, website }
    });
};
