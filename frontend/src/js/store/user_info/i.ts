import { IAction } from '../i';

// state
export interface IUserInfoState {
    userInfo: any;
    changePasswordSuccess: boolean;
    changePasswordFail: boolean;
}

// actions
export interface IUserInfoActions {
    getUserInfo?: IGetUserInfo;
    updateUserInfo?: IUpdateUserInfo;
}
export enum ActionTypes {
    GET_USER_INFO = 'GET_USER_INFO',
    UPDATE_USER_INFO = 'UPDATE_USER_INFO',
    CHANGE_PASSWORD = 'CHANGE_PASSWORD',
    RESET_PASSWORD = 'RESET_PASSWORD',
}
// other
export type IGetUserInfo = IAction<{}, any>;
export type IUpdateUserInfo = IAction<{ firstName?: string, lastName?: string, phone?: string, email?: string }, any>;

