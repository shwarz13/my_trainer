import Sequelize from "sequelize";
import { BASE_INSTANCE_FIELDS } from "../../../../database/constants";

const schema = {
  ...BASE_INSTANCE_FIELDS,
  set: {
    type: Sequelize.UUID,
    allowNull: false
  },
  parameter: {
    type: Sequelize.UUID,
    allowNull: false
  },
  measure: {
    type: Sequelize.UUID
  },
  plan: {
    type: Sequelize.DOUBLE,
    allowNull: false
  },
  fact: {
    type: Sequelize.DOUBLE
  }
};

export default schema;
