import { routeName } from "../constants/";
import Joi from "@hapi/joi";

const measureSchema = Joi.object({
  id: Joi.string().required(),
  name: Joi.string().required(),
});

const parameterSchema = Joi.object({
  id: Joi.string().required(),
  name: Joi.string().required(),
  required: Joi.boolean().required(),
  measureRequired: Joi.boolean().required(),
  measures: Joi.array()
    .items(measureSchema)
    .required(),
});

const exerciseSchema = Joi.object({
  id: Joi.string().required(),
  name: Joi.string().required(),
  description: Joi.string().optional(),
  media: Joi.object().optional(),
  parameters: Joi.array()
    .items(parameterSchema)
    .required(),
});

const sportSchema = Joi.object({
  id: Joi.string().required(),
  name: Joi.string().required(),
  description: Joi.string().optional(),
  exercises: Joi.array()
    .items(exerciseSchema)
    .required(),
});

const metaResponseSchema = Joi.object({
  sports: Joi.array()
    .items(sportSchema)
    .required(),
});

const userSchema = Joi.object({
  id: Joi.string().required(),
  firstName: Joi.string().required(),
  lastName: Joi.string().required(),
  secondName: Joi.string(),
  phone: Joi.string(),
  email: Joi.string(),
  avatar: Joi.string(),
});

const singleTraining = Joi.object({
  id: Joi.string().required(),
  date: Joi.date().required(),
  place: Joi.string(),
  trainerPerson: userSchema,
  sportsmanPerson: userSchema,
  name: Joi.string().required(),
  comment: Joi.string().optional(),
  createdAt: Joi.date().required(),
  updatedAt: Joi.date().required(),
});

const trainingItem = Joi.object({
  id: Joi.string().required(),
  createdAt: Joi.date().required(),
  date: Joi.date().required(),
  place: Joi.string().required(),
  name: Joi.string().required(),
  comment: Joi.string().allow(null),
  tasks: Joi.array().items(
    Joi.object({
      id: Joi.string().required(),
      comment: Joi.string().allow(null),
      exerciseItem: Joi.object({
        id: Joi.string().required(),
        name: Joi.string().required(),
      }).required(),
      sets: Joi.array()
        .items(
          Joi.object({
            id: Joi.string().required(),
            order: Joi.number().required(),
            setParameterRelations: Joi.array()
              .items(
                Joi.object({
                  id: Joi.string().required(),
                  plan: Joi.number().required(),
                  fact: Joi.number().allow(null),
                  parameterItem: Joi.object({
                    id: Joi.string().required(),
                    name: Joi.string().required(),
                    description: Joi.string().allow(null),
                  }).required(),
                  measureItem: Joi.object({
                    id: Joi.string().required(),
                    name: Joi.string().required(),
                    description: Joi.string().allow(null),
                  }).allow(null),
                }).required()
              )
              .required(),
          }).required()
        )
        .required(),
    })
  ),
});

const trainingListResponse = Joi.array().items(singleTraining);

export const list = {
  description: "Получить список тренировок",
  // notes: "asdfgh",
  tags: ["api", routeName],
  plugins: {
    "hapi-swagger": {
      validate: {},
    },
  },
  response: {
    schema: trainingListResponse,
    failAction: "log",
  },
};

export const meta = {
  description: "Получить список справочников для тренировок",
  // notes: "asdfgh",
  tags: ["api", routeName],
  plugins: {
    "hapi-swagger": {
      validate: {},
      security: [{ Bearer: [] }],
    },
  },
  response: { schema: metaResponseSchema, failAction: "log" },
};

export const newTraining = {
  description: "Создание новой тренировки",
  tags: ["api", routeName],
  plugins: {
    "hapi-swagger": {
      validate: {},
      security: [{ Bearer: [] }],
    },
  },
  response: { schema: trainingListResponse, failAction: "log" },
};

export const getItem = {
  description: "Получение тренировки",
  tags: ["api", routeName],
  plugins: {
    "hapi-swagger": {
      validate: {},
      security: [{ Bearer: [] }],
    },
  },
  response: {
    schema: trainingItem,
    // TODO: проверить почему неправильный ответ от сервера и он не проходит обратную валидацию
    failAction: "log",
  },
};

export const completeTraining = {
  description: "Выполнение упражнений в трренировке",
  tags: ["api", routeName],
  plugins: {
    "hapi-swagger": {
      validate: {},
      security: [{ Bearer: [] }],
    },
  },
  response: {
    schema: trainingItem,
    // TODO: проверить почему неправильный ответ от сервера и он не проходит обратную валидацию
    failAction: "log",
  },
};
