describe('The Login Page', function () {
    it('sets auth cookie when logging in via form submission', function () {    
        cy.visit('/')
        cy.get('.auth-link').click();

        cy.get('input[name="name"]').type(`3`)
        cy.get('input[name="password"]').type(`4`)
        cy.get('button[type=submit]').click();

        cy.get('h1').should('contain', 'Проекты')

        // auth cookie should be present
        cy.getCookie('token').should('exist');
        cy.getCookie('refreshToken').should('exist');
        cy.getCookie('expiresIn').should('exist');
    })
})
