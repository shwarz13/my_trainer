import { QueryInterface } from "sequelize";
import AuthToolkit from "../../utils/AuthToolkit";

const user = {
  id: "2e6faeed-5d73-47f3-88e9-51d88b1c2bce",
  first_name: "firstname",
  last_name: "lastname",
  email: "test@test.ru",
  password: AuthToolkit.getHash("12345678"),
  created_at: new Date(),
  updated_at: new Date()
};

const TABLE = "users";

module.exports = {
  up: (queryInterface: QueryInterface) =>
    queryInterface.bulkInsert(TABLE, [user]),

  down: async (queryInterface: QueryInterface) =>
    queryInterface.bulkDelete(TABLE, { id: user.id })
};
