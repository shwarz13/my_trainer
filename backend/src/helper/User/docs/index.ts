import { routeName } from "../constants/";
import Joi = require("@hapi/joi");

const userJSONSchema = Joi.object({
  id: Joi.string().required(),
  phone: Joi.string()
    .optional()
    .allow(null),
  email: Joi.string()
    .optional()
    .allow(null),
  firstName: Joi.string().required(),
  lastName: Joi.string().required(),
  avatar: Joi.string()
    .optional()
    .allow(null),
  socials: Joi.object()
    .optional()
    .allow(null)
});

export const read = {
  description: "Получить список пользователей",
  tags: ["api", routeName],
  plugins: {
    "hapi-swagger": {
      validate: {}
    }
  }
};

export const about = {
  description: "Получить информацию о текущем пользователе",
  tags: ["api", routeName],
  plugins: {
    "hapi-swagger": {
      validate: {},
      security: [{ Bearer: [] }]
    }
  },
  response: {
    schema: userJSONSchema
  }
};

export const update = {
  description: "Обновить данные текущего пользователя",
  tags: ["api", routeName],
  plugins: {
    "hapi-swagger": {
      validate: {},
      security: [{ Bearer: [] }]
    }
  },
  response: {
    schema: userJSONSchema
  }
};

// export const del = {
//   description: "Удалить пользователя",
//   tags: ["api", routeName],
//   plugins: {
//     "hapi-swagger": {
//       validate: {}
//     }
//   }
// };
