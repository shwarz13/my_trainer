import { server } from "../server";
import Boom from "@hapi/boom";
import Logger from "../utils/Logger";
import SystemError from "./SystemError";
import errors from "../constants/errors";

interface ITrycatcherOptions {
  logMessage?: string;
  isRequest?: boolean;
  isThrow?: boolean;
  logFunction?: any;
}

export default function trycatcher(
  f: (...params) => void,
  optionsTC: ITrycatcherOptions
) {
  const options = {
    logMessage: "Undescripted message error",
    isRequest: false,
    isThrow: true,
    ...optionsTC
  };
  return async function(...params): Promise<any | Boom> {
    try {
      const res = await f(...params);
      return res;
    } catch (error) {
      console.log({ error });
      Logger.debug(`${options.logMessage} -----> ${error}`);
      if (options.isRequest) {
        if (SystemError.isSystemError(error)) {
          return server.generateHttpError({
            code: error.code,
            message: error.message
          });
        }
        return server.generateHttpError(errors.unknownError);
      } else {
        if (options.isThrow) {
          throw error;
        } else {
          return error;
        }
      }
    }
  };
}
