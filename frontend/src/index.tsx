import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import {store, history} from './js/store';
import App from './js/components/App';

import 'Styles/index.scss';

ReactDOM.render(
	<Provider {...{store}}>
		<ConnectedRouter {...{store, history}}>
			<App />
		</ConnectedRouter>
	</Provider>,
	document.getElementById('root')
);
