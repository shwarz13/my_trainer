import { combineReducers } from 'redux';

import header from './header/reducer';

import auth from './auth/reducer';
import register from './registration/reducer';
import loaded from './loaded';
import userInfo from './user_info/reducer';
import mainInterface from './interface/reducer';
import exercises from './exercise/reducer';

export default combineReducers({
    header,
    auth,
    register,
    loaded,
    userInfo,
    mainInterface,
    exercises
});
