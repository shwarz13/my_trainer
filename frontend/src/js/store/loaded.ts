import { AnyAction } from 'redux';

import { RequestState } from 'Constants';
import { ILoadingState, LoadingStatus, IAction } from './i';

// ACTION
export enum AT {
  REFRESH_LOADING_STATE = 'REFRESH_LOADING_STATE',
}

export type IRefreshLoadingState = IAction<{ actionType: string }, any>;
export const refreshLoadingState: IRefreshLoadingState = ({ actionType }) => {
  return dispatch =>
    dispatch({
      type: AT.REFRESH_LOADING_STATE,
      actionType
    });
};

// REDUCER
const initialState: ILoadingState = {
  objects: {}
};

export default (state = initialState, { type, actionType, other }: AnyAction) => {
  if (type.includes(RequestState.REQUEST)) {
    return { ...state, [actionType]: LoadingStatus.REQUEST };
  }
  if (type.includes(RequestState.SUCCESS)) {
    if (type.includes('GET_OBJECT_INFO')) {
      return {
        ...state,
        objects: {
          ...(state.objects as object),
          [other.GUID]: LoadingStatus.SUCCESS
        }
      };
    }
    return { ...state, [actionType]: LoadingStatus.SUCCESS };
  }
  if (type.includes(RequestState.FAIL)) {
    return { ...state, [actionType]: LoadingStatus.FAIL };
  }
  if (type === AT.REFRESH_LOADING_STATE) {
    return { ...state, [actionType]: LoadingStatus.NONE };
  }
  // if (type === '@@router/LOCATION_CHANGE') {
  //     return {objects: state.objects};
  // }
  return state;
};
