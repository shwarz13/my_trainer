import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import * as History from 'history';
import thunk from 'redux-thunk';
import reducer from './store/index';

export const middlewares = [
	thunk,
];

export const history = History.createHashHistory();

const isDev = process.env.NODE_ENV !== `production`;
const enhancer = isDev
  ? composeWithDevTools(applyMiddleware(...middlewares))
  : applyMiddleware(...middlewares);

export const store = createStore(reducer, enhancer);
