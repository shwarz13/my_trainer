export const initialPath = '/';
export const MAIN_URL = process.env.MAIN_URL; // https://wger.de/api/v2/

export enum RequestState {
  REQUEST = '_REQUEST',
  SUCCESS = '_SUCCESS',
  FAIL = '_FAIL',
}
export enum RequestType {
  GET = '_GET_REQUEST',
  POST = '_POST_REQUEST',
  PUT = '_PUT_REQUEST',
  DELETE = '_DELETE_REQUEST',
}

export const requestTimeout =
process.env.REQUEST_TIMEOUT ? parseInt(process.env.REQUEST_TIMEOUT, 10) : 60000;

export const cookiePrefix = process.env.COOKIE_PREFIX || '';
export const storagePrefix = process.env.STORAGE_PREFIX || '';
export const cookieTokenLifeTime = parseInt(
  process.env.REACT_APP_COOKIE_TOKEN_LIFETIME || '',
  10
);

interface IProcess extends NodeJS.Process {
  browser?: boolean;
}

export const PROCESS: IProcess = process;



