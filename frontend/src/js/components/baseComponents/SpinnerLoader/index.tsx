import React, { useEffect, useState } from 'react';
import { hot } from 'react-hot-loader';
import { connect } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';

// styles
import './index.scss';



const Spinner = () => {

	const useStyles = makeStyles(theme => ({
		colorSecondary : {
			color: '#FF782D'
		}
	}));

	const classes = useStyles();

	return (
		<div className="spinner">
			<div className="spinner__bg"/>
			<CircularProgress color="secondary" size={50} className={classes.colorSecondary} />
		</div>
	);
};


export default hot(module)(
	connect(
		null,
		null
	)(Spinner)
);
