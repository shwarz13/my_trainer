import Sequelize from "sequelize";
import { IExerciseInstance, IExercise, IExerciseBase } from "../interfaces";
import ExerciseSchema from "./models";
import { BASE_INSTANCE_PARAMS } from "../../../database/constants";

export default (sequelize: Sequelize.Sequelize) => {
  const instance = sequelize.define<
    IExerciseInstance,
    IExercise,
    IExerciseBase
  >("exercise", ExerciseSchema, {
    ...BASE_INSTANCE_PARAMS
  });

  return instance;
};
