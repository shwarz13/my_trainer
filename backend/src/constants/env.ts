// Projecct env
export const API_VERSION = process.env.API_VERSION || "v1";
export const SERVER_PORT = parseInt(process.env.SERVER_PORT || "3000", 10);
export const APP_STATUS = process.env.APP_STATUS || "dev";
export const SECRET_TOKEN = process.env.SECRET_TOKEN || "somesecret";
export const ADMIN_SECRET_TOKEN =
  process.env.ADMIN_SECRET_TOKEN || "adminsomesecret";
export const RESETPASS_SECRET_TOKEN =
  process.env.RESETPASS_SECRET_TOKEN || "resetpasssomesecret";
export const CLIENT_URL = process.env.CLIENT_URL || "http://localhost:8080";
// DB env
export const DB = process.env.DB || "trainer";
export const DBUSERNAME = process.env.DBUSERNAME || "trainer_dbadmin";
export const DBPASSWORD = process.env.DBPASSWORD || "123456";
export const DBHOST = process.env.DBHOST || "localhost";
export const DBPORT = parseInt(process.env.DBPORT || "5432", 10);
// Redis env
export const REDIS_URL = process.env.REDIS_URL || "redis://redis:6379/0";
