import Sequelize from "sequelize";
import { BASE_INSTANCE_FIELDS } from "../../../../database/constants";

const schema = {
  ...BASE_INSTANCE_FIELDS,
  firstName: {
    type: Sequelize.STRING(50),
    allowNull: false,
    field: "first_name"
  },
  lastName: {
    type: Sequelize.STRING(50),
    allowNull: false,
    field: "last_name"
  },
  phone: {
    type: Sequelize.STRING(10)
  },
  email: {
    type: Sequelize.STRING(100),
    unique: true
  },
  password: {
    type: Sequelize.STRING(256),
    allowNull: false
  },
  avatar: {
    type: Sequelize.STRING(1024)
  },
  socials: {
    type: Sequelize.JSON
  }
};

export default schema;
