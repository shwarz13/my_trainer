import { RedisClient } from "redis";

class RedisService {
  private _redisClient: RedisClient;

  constructor(redisClient) {
    // this._redisClient = redisClient;
    this._redisClient = redisClient;
  }
  protected async get(query: string): Promise<string | null> {
    // @ts-ignore
    return this._redisClient.getAsync(query);
  }

  protected async del(query: string): Promise<boolean> {
    return this._redisClient.del(query);
  }

  // expiresIn указывается в секундах
  protected async set(query: string, data: string | number, expiresIn: number) {
    const params = [query, data];
    if (expiresIn) {
      params.push("EX", expiresIn);
    }
    // @ts-ignore
    return this._redisClient.set(...params);
  }
}

export default RedisService;
