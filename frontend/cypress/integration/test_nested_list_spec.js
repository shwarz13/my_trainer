describe('The Home Page', function() {
    it('successfully loads', function() {
        cy.visit('/')
        cy.get('.auth-link').click();

        cy.get('input[name="name"]').type(`3`)
        cy.get('input[name="password"]').type(`4`)
        cy.get('button[type=submit]').click();

        cy.get('h1').should('contain', 'Проекты')

        cy.get(':nth-child(5) > .sidebar-nav-link').click();
        cy.get(':nth-child(5) > .project > .project_show').click();
        cy.get(':nth-child(1) > :nth-child(1) > .sidebar-nav-link').click();
        cy.get('.top-menu > :nth-child(2) > .sidebar-nav-link').click();
        cy.get(':nth-child(3) > .element_title > .element_open').click();
        cy.contains('02.1');
    })
});
