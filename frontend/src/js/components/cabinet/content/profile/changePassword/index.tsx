import React, { Fragment, useEffect, useState } from 'react';
import { hot } from 'react-hot-loader';
import { connect } from 'react-redux';

// styles
import './index.scss';
import { setLeftBtn, setRightBtn } from 'Store/header/actions';
import { IHeaderActions } from 'Store/header/i';
import Input from 'Components/baseComponents/Input';

type IProps = {
	history?: any;
} & IHeaderActions;

const ChangePasswordComponent = (props: IProps) => {

	const [oldPassword, setOldPassword] = useState('');
	const [newPassword, setNewPassword] = useState('');
	const [newRepeatPassword, setNewRepeatPassword] = useState('');

	useEffect(() => {
		props.setLeftBtn({isShow: true, text: 'Назад', func: () => props.history.goBack()});
		props.setRightBtn({isShow: true, text: 'Сохранить', func: () => console.log('SAVE~!!!!')});
		}, []);

	return (
		<div className="change-password-inputs">
			<Input value={oldPassword} onChange={value => setOldPassword(value)} placeholder={'Старый пароль'}/>
			<Input value={newPassword} onChange={value => setNewPassword(value)} placeholder={'Новый пароль'}/>
			<Input value={newRepeatPassword} onChange={value => setNewRepeatPassword(value)} placeholder={'Повторите новый пароль'}/>
		</div>
	);
};


export default hot(module)(
	connect(
		null,
		{ setLeftBtn, setRightBtn }
	)(ChangePasswordComponent)
);
