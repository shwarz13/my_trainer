import Sequelize from "sequelize";
import {
  IExerciseParameterRelationInstance,
  IExerciseParameterRelation,
  IExerciseParameterRelationBase
} from "../interfaces";
import ExerciseParameterRelationSchema from "./models";
import { BASE_INSTANCE_PARAMS } from "../../../database/constants";

export default (sequelize: Sequelize.Sequelize) => {
  const instance = sequelize.define<
    IExerciseParameterRelationInstance,
    IExerciseParameterRelation,
    IExerciseParameterRelationBase
  >("exercise_parameter_relation", ExerciseParameterRelationSchema, {
    ...BASE_INSTANCE_PARAMS
  });

  return instance;
};
