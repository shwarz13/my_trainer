import Boom from "@hapi/boom";

import { IDecoratedRequest } from "../../../interfaces/index";
import logger from "../../../utils/Logger";

import { ISetBase, ISet, ISetInstance } from "../interfaces";

import methods from "../";
import { routeName } from "../constants";

const ctrl = {};
export default ctrl;
