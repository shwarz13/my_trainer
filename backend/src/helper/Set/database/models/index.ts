import Sequelize from "sequelize";
import { BASE_INSTANCE_FIELDS } from "../../../../database/constants";

const schema = {
  ...BASE_INSTANCE_FIELDS,
  task: {
    type: Sequelize.UUID,
    allowNull: false
  },
  order: {
    type: Sequelize.INTEGER,
    allowNull: false
  }
};

export default schema;
