import { HTTP_ERROR_CODES } from ".";

export default {
  unknownError: {
    code: HTTP_ERROR_CODES.BAD_REQUEST,
    message:
      "Произошла странная ошибка. Мы над ней уже работаем, не переживайте"
  }
};
