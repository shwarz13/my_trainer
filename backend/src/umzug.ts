import Umzug, { UmzugOptions } from "umzug";
import { Sequelize } from "sequelize";
import logger from "./utils/Logger";

function logUmzugEvent(eventName) {
  return function(name, migration) {
    logger.debug(`${name} ${eventName}`);
  };
}

export default sequelize => {
  const migrationConfig: UmzugOptions = {
    storage: "sequelize",
    downName: "down",
    upName: "up",
    logging: false,

    storageOptions: {
      sequelize,
      tableName: "sequelize_meta"
    },

    migrations: {
      params: [sequelize.getQueryInterface(), Sequelize],
      path: `${__dirname}`,
      // @ts-ignore
      traverseDirectories: true,
      pattern: /migration\.ts$/
    }
  };

  const seederConfig: UmzugOptions = {
    ...migrationConfig,
    storageOptions: {
      // ...migrationConfig.storageOptions,
      sequelize,
      tableName: "sequelize_data",
      modelName: "SequelizeData"
    },
    migrations: {
      ...migrationConfig.migrations,
      pattern: /seeder\.ts$/
    }
  };

  const instances = {
    Migration: new Umzug(migrationConfig),
    Seeder: new Umzug(seederConfig)
  };

  Object.keys(instances).forEach(key => {
    instances[key].on("migrating", logUmzugEvent("migrating"));
    instances[key].on("migrated", logUmzugEvent("migrated"));
    instances[key].on("reverting", logUmzugEvent("reverting"));
    instances[key].on("reverted", logUmzugEvent("reverted"));
  });

  return instances;
};
