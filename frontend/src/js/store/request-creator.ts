import axios from 'axios';
import { Dispatch } from 'redux';
import {
  RequestState,
  RequestType,
} from 'Constants';
import instance from './interceptor'
import { IRequestCreatorAction } from './i';

export async function requestCreator(
  dispatch: Dispatch,
  action: IRequestCreatorAction
): Promise<any> {
  const {
    type,
    requestType,
    requestUrl: url,
    resultField = 'data',
    sendObject,
    sendParams,
    other,
  } = action;

  console.log(type + RequestState.REQUEST, {
    type: type + RequestState.REQUEST,
    url,
    sendObject,
    sendParams,
  });


  dispatch({
    type: type + RequestState.REQUEST,
    actionType: type,
    requestType,
    other,
  });
  let method;
  let data;
  let params;
  switch (requestType) {
    case RequestType.GET: {
      method = 'get';
      params = sendParams;
      break;
    }
    case RequestType.POST: {
      method = 'post';
      data = sendObject;
      break;
    }
    case RequestType.PUT: {
      method = 'put';
      data = sendObject;
      break;
    }
    case RequestType.DELETE: {
      method = 'delete';
      params = sendParams;
      data = sendObject;
      break;
    }
    default: {
      console.log(`${requestType} - неизвестный тип запроса`);
      return;
    }
  }
  return instance({
    url,
    method,
    data,
    params
  })
    .then(result => {
      dispatch({
        type: type + RequestState.SUCCESS,
        payload: result[resultField],
        fullResponse: result,
        actionType: type,
        requestType,
        other,
      });
      console.log('RESULT IS OK', type);
      return result[resultField];
    })
    .catch(error => {
      console.log(
        JSON.stringify(
          {
            type: type + RequestState.FAIL,
            msg: error.message,
            fullResponse: error,
          },
          null,
          2
        )
      );
      dispatch({
        type: type + RequestState.FAIL,
        msg: error.message,
        fullResponse: error,
        actionType: type,
        requestType,
        error,
        other,
      });
      return error.message;
    });
}

