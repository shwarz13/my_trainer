import React, {Fragment} from 'react';
import { hot } from 'react-hot-loader';
import { connect } from 'react-redux';

// store
import { IHeaderActions, IHeaderState } from 'Store/header/i';

// styles
import './index.scss';

type IProps = {
	title?: string;
} & IHeaderState & IHeaderActions;

const Header = (props: IProps) => {

	const { leftBtn, rightBtn } = props;
	return (
		<header className={'header'}>
			{
				leftBtn.isShow ?
					<span className="header__btn" onClick={() => leftBtn.func()}>{leftBtn.text || leftBtn.component || ''}</span> :
					null
			}
			<span className={'header__title'}>{props.title || ''}</span>
			{
				rightBtn.isShow ?
					<Fragment>
						{leftBtn.isShow ? null : <span/>}
						<span className="header__btn" onClick={rightBtn.func}>{rightBtn.text || rightBtn.component || ''}</span>
					</Fragment> :
					null
			}
		</header>
	);
};

const mapStateToProps = (state) => {
	return  {
		leftBtn: state.header.leftBtn,
		rightBtn: state.header.rightBtn
	}
};

export default hot(module)(
	connect(
		mapStateToProps,
		null
	)(Header)
);
