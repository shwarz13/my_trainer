import logger from "../../utils/Logger";

import { Op, Transaction } from "sequelize";
import { INewSet } from "./interfaces";
import { server } from "../../server";

import SetParameterMethods from "../SetParameterRelation";
import SystemError from "../../utils/SystemError";

class Methods {
  public static async read() {
    return [];
  }

  public static async create(
    sets: INewSet[],
    incomingTransaction?: Transaction
  ) {
    const transaction =
      incomingTransaction || (await server.dbConnect.transaction());
    try {
      const result = await Promise.all(
        sets.map(async ({ setParameters, ...set }) => {
          const setInstance = await server.Set.create(set, { transaction });
          await SetParameterMethods.create(
            setParameters.map(setParameter => ({
              ...setParameter,
              set: setInstance.id
            })),
            transaction
          );
        })
      );
      if (!incomingTransaction) {
        await transaction.commit();
      }
    } catch (error) {
      if (!incomingTransaction) {
        await transaction.rollback();
      }
      if (error instanceof SystemError) {
        throw error;
      }
      throw error;
    }
  }
}

export default Methods;
