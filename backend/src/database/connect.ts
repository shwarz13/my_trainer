import Sequelize from "sequelize";

import { DB, DBHOST, DBPASSWORD, DBPORT, DBUSERNAME } from "../constants/env";

const seq = new Sequelize.Sequelize(DB, DBUSERNAME, DBPASSWORD, {
  host: DBHOST,
  port: DBPORT,
  dialect: "postgres",
  operatorsAliases: false,
  timezone: "+00:00",
  logging: false
});

export default seq;
