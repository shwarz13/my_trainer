import { AnyAction } from 'redux';
import { IMainInterfaceState, ActionTypes as AT } from './i';

const initialState: IMainInterfaceState = {
  isSpinnerShow: false
};

export default (state = initialState, action: AnyAction) => {
  switch (action.type) {
    case AT.SHOW_SPINNER: {
      return {
        ...state,
        isSpinnerShow: action.other.show
      };
    }
    default: {
      return state;
    }
  }
};
