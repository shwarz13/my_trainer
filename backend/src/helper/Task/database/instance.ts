import Sequelize from "sequelize";
import { ITaskInstance, ITask, ITaskBase } from "../interfaces";
import TaskSchema from "./models";
import { BASE_INSTANCE_PARAMS } from "../../../database/constants";

export default (sequelize: Sequelize.Sequelize) => {
  const instance = sequelize.define<ITaskInstance, ITask, ITaskBase>(
    "task",
    TaskSchema,
    {
      ...BASE_INSTANCE_PARAMS
    }
  );

  return instance;
};
