import { AnyAction } from 'redux';
import { IHeaderState, ActionTypes as AT } from './i';


const initialState: IHeaderState = {
  headerTitle: 'fff',
  leftBtn: {
    isShow: false,
    text: '',
    func: null,
    component: null
  },
  rightBtn: {
    isShow: false,
    text: '',
    func: null,
    component: null
  }
};

export default (state = initialState, action: AnyAction) => {
  switch (action.type) {
    case AT.HEADER_TITLE: {
      return {
        ...state,
        headerTitle: action.other.title
      };
    }
    case AT.SET_LEFT_BTN: {
      return {
        ...state,
        leftBtn: {
          isShow: action.payload.isShow,
          text: action.payload.text,
          func: action.payload.func,
          component: action.payload.component
        }
      }
    }
    case AT.SET_RIGHT_BTN: {
      return {
        ...state,
        rightBtn: {
          isShow: action.payload.isShow,
          text: action.payload.text,
          func: action.payload.func,
          component: action.payload.component
        }
      }
    }
    default: {
      return state;
    }
  }
};
