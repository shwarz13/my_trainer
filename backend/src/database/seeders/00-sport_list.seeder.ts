import { QueryInterface } from "sequelize";
import { ISportInstance } from "../../helper/Sport/interfaces";
import { IExerciseInstance } from "../../helper/Exercise/interfaces";
import { IParameterInstance } from "../../helper/Parameter/interfaces";
import { IMeasureInstance } from "../../helper/Measure/interfaces";

const baseHardAthleticParameters = [
  {
    name: "Повторения",
    required: true,
    measureRequired: true,
    measures: [{ name: "шт" }]
  },
  {
    name: "Вес снаряда",
    required: true,
    measureRequired: true,
    measures: [{ name: "кг" }]
  }
];
const baseRunParameters = [
  {
    name: "Дистанция",
    required: true,
    measureRequired: true,
    measures: [{ name: "м" }, { name: "км" }]
  },
  {
    name: "Темп",
    required: false,
    measureRequired: false,
    measures: []
  }
];
const elements = [
  {
    name: "Тяжелая атлетика / тренажерный зал",
    exercises: [
      {
        name: "Жим штанги лежа на горизонтальной скамье",
        parameters: baseHardAthleticParameters
      },
      {
        name: "Жим гантель лежа, в позитивном наклоне",
        description: "В качестве веса снаряда указан вес одной гантели",
        parameters: baseHardAthleticParameters
      },
      {
        name: "Присед",
        parameters: baseHardAthleticParameters
      },
      {
        name: "Становая тяга",
        parameters: baseHardAthleticParameters
      }
    ]
  },
  {
    name: "Бег",
    exercises: [
      {
        name: "Спринт",
        parameters: baseRunParameters
      },
      {
        name: "Бег на дальние дистанции",
        parameters: baseRunParameters
      }
    ]
  }
];

module.exports = {
  up: (queryInterface: QueryInterface) =>
    queryInterface.sequelize.transaction<any>({}, async transaction => {
      for (const sport of elements) {
        const sportInstance: ISportInstance = await queryInterface.sequelize.models.sport.create(
          { name: sport.name },
          { returning: true, transaction }
        );
        for (const exercise of sport.exercises) {
          const exerciseInstance: IExerciseInstance = await queryInterface.sequelize.models.exercise.create(
            {
              name: exercise.name,
              sport: sportInstance.id
            },
            { transaction, returning: true }
          );
          for (const parameter of exercise.parameters) {
            const parameterInstance: IParameterInstance = (
              await queryInterface.sequelize.models.parameter.findOrCreate({
                where: {
                  name: parameter.name
                },
                defaults: {
                  name: parameter.name,
                  measureRequired: parameter.measureRequired
                },
                transaction
              })
            )[0];
            // @ts-ignore
            await exerciseInstance.addParameter(parameterInstance, {
              through: {
                required: parameter.required
              },
              transaction
            });
            for (const measure of parameter.measures) {
              const measureInstance: IMeasureInstance = (
                await queryInterface.sequelize.models.measure.findOrCreate({
                  where: {
                    name: measure.name
                  },
                  defaults: {
                    name: measure.name
                  },
                  transaction
                })
              )[0];
              // @ts-ignore
              await parameterInstance.addMeasure(measureInstance, {
                transaction
              });
            }
          }
        }
      }

      return true;
    }),

  down: async (queryInterface: QueryInterface) => {
    return true;
  }
};
