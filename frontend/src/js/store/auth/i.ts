import { IAction } from '../i';

// state
export interface IAuthState {
    loggingIn?: boolean;
    loggedIn?: boolean;
    token?:string;
}

// actions
export interface IAuthActions {
    clientLogin?: IClientLogin;
    clientLogout?: IClientLogout;
    setSession?: ISetSession;
}
export enum ActionTypes {
    CLIENT_LOGIN = 'CLIENT_LOGIN',
    CLIENT_LOGOUT = 'CLIENT_LOGOUT',
    SET_SESSION = 'SET_SESSION',
}

// other
export type IClientLogin = IAction<{ login: string; password: string }, any>;
export type IClientLogout = IAction<{}, any>;
export type ISetSession = IAction<{ token: string }, any>;


