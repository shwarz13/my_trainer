import { IBaseInstanceFields } from "../../../interfaces";
import Sequelize from "sequelize";
import { INewSet } from "../../Set/interfaces";

/* *
 *   Task
 * */

export interface ITaskBase {
  training: string;
  exercise: string;
  comment: string;
}

export interface ITask extends ITaskBase, IBaseInstanceFields {}

export type ITaskInstance = Sequelize.Instance<ITask> & ITask;

export interface INewTask extends ITaskBase {
  sets: INewSet[];
}
