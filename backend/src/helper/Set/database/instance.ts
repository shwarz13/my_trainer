import Sequelize from "sequelize";
import { ISetInstance, ISet, ISetBase } from "../interfaces";
import SetSchema from "./models";
import { BASE_INSTANCE_PARAMS } from "../../../database/constants";

export default (sequelize: Sequelize.Sequelize) => {
  const instance = sequelize.define<ISetInstance, ISet, ISetBase>(
    "set",
    SetSchema,
    {
      ...BASE_INSTANCE_PARAMS
    }
  );

  return instance;
};
