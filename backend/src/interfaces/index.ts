import Sequelize from "sequelize";
import Hapi from "hapi";

export interface IBaseInstanceFields {
  id: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date;
}

export type IDecoratedRequest<P = {}, Q = {}, Par = {}, C = {}, H = {}> = {
  payload: P;
  query: Q;
  auth: { credentials: C };
  headers: H;
  params: Par;
} & Hapi.Request;
