import Sequelize from "sequelize";
import { BASE_INSTANCE_FIELDS } from "../../../../database/constants";

const schema = {
  ...BASE_INSTANCE_FIELDS,
  training: {
    type: Sequelize.UUID,
    allowNull: false
  },
  exercise: {
    type: Sequelize.UUID,
    allowNull: false
  },
  comment: {
    type: Sequelize.STRING(255)
  }
};

export default schema;
