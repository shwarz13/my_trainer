import Sequelize from "sequelize";
import { ITrainingInstance, ITraining, ITrainingBase } from "../interfaces";
import TrainingSchema from "./models";
import { BASE_INSTANCE_PARAMS } from "../../../database/constants";

export default (sequelize: Sequelize.Sequelize) => {
  const instance = sequelize.define<
    ITrainingInstance,
    ITraining,
    ITrainingBase
  >("training", TrainingSchema, {
    ...BASE_INSTANCE_PARAMS
  });

  return instance;
};
