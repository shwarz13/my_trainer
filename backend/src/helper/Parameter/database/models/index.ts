import Sequelize from "sequelize";
import { BASE_INSTANCE_FIELDS } from "../../../../database/constants";
import { IParameterInstance, IParameter } from "../../interfaces";

const schema = {
  ...BASE_INSTANCE_FIELDS,
  name: {
    type: Sequelize.STRING,
    allowNull: false
  },
  description: {
    type: Sequelize.STRING
  },
  measureRequired: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: false,
    field: "measure_required"
  }
};

export default schema;
