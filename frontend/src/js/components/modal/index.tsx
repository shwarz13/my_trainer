import React, { useRef, useState } from 'react';
import { hot } from 'react-hot-loader';

// libs
import Select, { components } from 'react-select';
// images
import Close from 'Images/close_modal.svg'
import ChevronBottom from 'Images/chevron-bottom.svg';
import CheckMark from 'Images/check-mark.svg';
import Paperclip from 'Images/paperclip.svg';
import DeleteIcon from 'Images/delete-icon.svg';

// styles
import './index.scss';

interface IProps {
	close: ()=>void;
	btn?: any;
	title?: string;
	content?: any;
	error?: ()=>void;
	requiredFields?: string[];
}

const ModalWindow = (props:IProps) => {

	const { btn, title, content, close, error, requiredFields } = props;

	const uploadInput = useRef();

	const [elementContent, setElement] = useState(content);
	const [uploadFile, setUploadFile] = useState(null);

	const customStylesForSelects = {
		container: styles => ({...styles,width: 540,fontSize: '16px',color: '#49585F',outline: 'none',}),
		indicatorSeparator: styles => ({...styles}),
		control: styles => ({...styles,border: '1px solid #E1E5E9',outline: 'none',boxShadow: 'none',borderRadius: 'none',cursor: 'pointer'}),
		indicatorsContainer: styles => ({...styles,width: 63,backgroundColor: '#fff'}),
		menuList: styles => ({...styles, marginTop: '0px', border: 'none', padding:0, maxHeight: 195}),
		menu: styles => ({...styles, marginTop: '1px', borderRadius: 'none',boxShadow: '0px 40px 60px rgba(145, 155, 159, 0.15)'}),
		option: (styles, {isFocused}) => ({...styles, backgroundColor: isFocused ? '#F4F6FB' : 'transparent', cursor: 'pointer', color: '#49585F'})
	};

	const DropdownIndicator = (props) => {
		return (
			<components.DropdownIndicator {...props}>
				<ChevronBottom/>
			</components.DropdownIndicator>
		);
	};

	const checkFields = () =>
		requiredFields ?
			requiredFields.some(field => elementContent.some(el => el.name === field && !el.value)) :
			false;

	const pasteElement = (element, index) => {
		const { type, name='', placeholder='', text='', action, value='', options=[], styles={}} = element;
		const elIndex = type === 'input' || type === 'select' || type === 'upload' ? elementContent.findIndex(el=>el.name===name) : 0;
		switch (true) {
			case type === 'input':
				return (
					<label className={`field ${name}`} key={`${name}-${text}-${index}`} style={styles}>
						<span>{text} {requiredFields && requiredFields.some(el=>el===name) && '*'}</span>
						<input
							style={styles}
							type="text"
							className={name}
							placeholder={placeholder}
							value={elementContent[elIndex].value}
							onChange={e=>{
								const elContent = [...elementContent];
								const elIndex = elContent.findIndex(el=>el.name===name) ;
								elContent[elIndex].value = e.target.value;
								setElement([ ...elContent ])
							}}/>
					</label>
				);
			case type === 'textarea':
				return (
					<label className={`field ${name}`} key={`${name}-${text}-${index}`}>
						<span>{text}</span>
						<textarea className={name} placeholder={placeholder} rows={4} value={value}/>
					</label>
				);
			case type === 'text':
				return (
					<div className={name + ' text_field'} key={`${name}-${text}-${index}`}>
						{text}
					</div>
				);
			case type === 'upload':
				return (
					<React.Fragment key={`${name}-${text}-${index}`}>
						<div className={name + ' link_field'} onClick={
							// @ts-ignore
							()=>uploadInput.current.click()
						}>
							<input type={'file'} className={'upload_input'} ref={uploadInput} onChange={e => {
								setUploadFile(e.target.files[0]);
								const newContent = [...elementContent];
								newContent[elIndex].value = {...e.target.files};
								setElement([...newContent]);
							}}/>
							<Paperclip/>
							<span className={'link'}>
								{text}
							</span>
							{
								uploadFile ?
									<div className={'upload_filename'}>{uploadFile.name} - {(uploadFile.size /1000).toFixed(1).toLocaleString()} Кб</div> :
									null
							}
						</div>

					</React.Fragment>
				);
			case type === 'select':
				const selectedOption = value && value.length ? options.reduce(el=>
					el.value === value && ({value, label: el.label})
				) : false;
				if(selectedOption) {
					const newContentElement = [...elementContent];
					newContentElement[elIndex].value = {...selectedOption};
					setElement([...newContentElement]);
				}
				return (
					<div className={`field ${name}`} key={`${name}-${text}-${index}`}>
						<span>{text} {requiredFields && requiredFields.some(el=>el===name) && '*'}</span>
						<Select
							value={elementContent[elIndex].value}
							onChange={selectedOption => {
								const newContent = [...elementContent];
								newContent[elIndex].value = {...selectedOption};
								setElement([...newContent]);
							}}
							options={options}
							styles={customStylesForSelects}
							classNamePrefix='modal-select'
							placeholder={placeholder ? placeholder : 'Выберите...'}
							components={{DropdownIndicator}}
						/>
					</div>

				);
			case type === 'submit':
				return (
					<button
						key={`${name}-${text}-${index}`}
						className="btn_send"
						type={'submit'}
						onClick={
							e => {
								e.preventDefault();
								if ( checkFields() ) {
									error();
								} else {
									action(elementContent);
									close();
								}
							}}
					>
						{text}<CheckMark/>
					</button>
				);
			case type === 'cancel':
				return (
					<button
						key={`${name}-${text}-${index}`}
						className="btn_cancel"
						type={'submit'}
						onClick={
							e => {
								e.preventDefault();
								action();
							}}
					>
						Отмена
					</button>
				);
			case type === 'delete':
				return (
					<button
						key={`${name}-${text}-${index}`}
						className="btn_delete"
						type={'submit'}
						onClick={
							e => {
								e.preventDefault();
								action();
								close();
							}}
					>
						<DeleteIcon/>Удалить
					</button>
				);
		}
	};

	return (
		<div className={'modal-window'}>
			<div className={'modal-window__overlay'}
					 onClick={
					 	(event)=> {
							// @ts-ignore
					 		if(event.target.classList.contains('modal-window__overlay')) {close();}}
					 }>
				<div className={'modal-window__content'}>
					<div className={'modal-window__content__title'}>{title}</div>
					<form action="" className={'modal-window__content__form'}>
						{
							content && content.length ?
								content.map((element, index) => pasteElement(element, index)) :
								null
						}
						{
							requiredFields && requiredFields.length ?
								<span className={'small required_fields'}>* - обязательные поля</span> :
								null
						}
						<div className={'modal-window__content__form__btn_line'}>
							{
								btn && btn.length ?
									btn.map((element, index) => pasteElement(element, index)) :
									null
							}
						</div>
					</form>
					<Close className={'modal-window__close'} onClick={close}/>
				</div>
			</div>
		</div>
	)
};

export default hot(module)(ModalWindow);
