import JWT from "jsonwebtoken";

import User from "../User";
import SystemError from "../../utils/SystemError";
import errors from "./constants/errors";
import userErrors from "../User/constants/errors";
import {
  SECRET_TOKEN,
  CLIENT_URL,
  RESETPASS_SECRET_TOKEN
} from "../../constants/env";
import uuid from "uuid";
import { server } from "../../server";
import { IUserJSON, ICreateUserPayload } from "../User/interfaces";
import { REFRESH_TOKEN_EXPIRATION } from "../../constants";
import { ILoginResponse, IResetPasswordPayload } from "./interfaces";
import UserMethods from "../User";
import AuthToolkit from "../../utils/AuthToolkit";
import moment from "moment";

export default class Auth {
  public static async login(
    login: string,
    password: string
  ): Promise<ILoginResponse> {
    const userInstance = await User.findByLogin(login, ["id", "password"]);
    if (
      !userInstance ||
      !AuthToolkit.isValidPassword(userInstance.password, password)
    ) {
      throw new SystemError(errors.userNotFound);
    }
    const userData = (userInstance.toJSON() as any) as IUserJSON;

    const { token, expiresIn } = this.generateJWT({ id: userData.id });

    return {
      token,
      refreshToken: await this.generateRefreshToken(userData.id),
      expiresIn
    };
  }

  public static async registration(
    userData: ICreateUserPayload
  ): Promise<boolean> {
    const userInstance = await UserMethods.create(userData);
    return !!userInstance;
  }

  public static async refresh(
    oldToken: string,
    refreshToken: string
  ): Promise<ILoginResponse> {
    const userData = (JWT.verify(oldToken, SECRET_TOKEN, {
      ignoreExpiration: true
    }) as any) as { id: string; iat: number; exp: number };

    const storedRefreshToken = await server.RedisService.get(
      `${userData.id}:refreshToken`
    );

    if (storedRefreshToken !== refreshToken) {
      throw new SystemError({
        ...errors.badRefreshToken
      });
    }

    const { token, expiresIn } = this.generateJWT({ id: userData.id });

    return {
      token,
      refreshToken: await this.generateRefreshToken(userData.id),
      expiresIn
    };
  }

  public static async resetPassword(
    userId,
    oldPassword,
    newPassword
  ): Promise<boolean> {
    const userInstance = await UserMethods.findUserById(userId, [
      "id",
      "password"
    ]);
    if (!userInstance) {
      throw new SystemError(userErrors.userNotFound);
    }
    if (!AuthToolkit.isValidPassword(userInstance.password, oldPassword)) {
      throw new SystemError(errors.compareOldPasswordAndHashError);
    }

    const result = await UserMethods.update(userId, { password: newPassword });
    if (!result[0]) {
      throw new SystemError(errors.passwordResetError);
    }

    return true;
  }

  public static async resetForgottenPassword(login: string): Promise<boolean> {
    const userInstance = await UserMethods.findByLogin(login);
    if (!userInstance) {
      throw new SystemError(userErrors.userNotFound);
    }
    const resetToken = uuid.v4();
    await server.RedisService.set(
      `${userInstance.id}:resetPasswordToken`,
      resetToken,
      60 * 60 * 24
    );
    const token = this.generateJWT(
      { id: userInstance.id, resetToken },
      RESETPASS_SECRET_TOKEN
    );
    // TODO: сделать отправку ссылки для сброса пароля на email
    const linkForReset = `${CLIENT_URL}/reset-password?token=${token}`;
    // TODO: удалить временное решение, выводящее в консоль URL для сброса пароля
    console.log(`[RESET TOKEN] ${linkForReset}`);
    return true;
  }

  public static async resetForgottenPasswordComplete(
    userId: string,
    resetToken: string,
    newPassword: string
  ): Promise<boolean> {
    const storedToken = await server.RedisService.get(
      `${userId}:resetPasswordToken`
    );
    const userInstance = await UserMethods.findUserById(userId);

    if (storedToken !== resetToken || !userInstance) {
      throw new SystemError(errors.passwordResetError);
    }

    userInstance.password = newPassword;
    await userInstance.save();

    return true;
  }

  public static async generateRefreshToken(userId): Promise<string> {
    const refreshToken = uuid.v4();
    await server.RedisService.set(
      `${userId}:refreshToken`,
      refreshToken,
      REFRESH_TOKEN_EXPIRATION
    );
    return refreshToken;
  }

  public static generateJWT(data: any, secret: string = SECRET_TOKEN) {
    return {
      token: JWT.sign(data, secret, {
        algorithm: "HS256",
        expiresIn: "24h"
      }),
      expiresIn: moment()
        .add(24, "hours")
        .unix()
    };
  }
}
