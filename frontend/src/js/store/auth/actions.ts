import { requestCreator } from '../request-creator';
import { MAIN_URL } from 'Constants';

import {
  IClientLogin,
  IClientLogout,
  ISetSession,
  ActionTypes as AT
} from './i';
import { RequestType } from 'Constants';

export const clientLogin: IClientLogin = ({ login, password }) => {
  return dispatch =>
    requestCreator(dispatch, {
      type: AT.CLIENT_LOGIN,
      requestUrl: `${MAIN_URL}auth/login`,
      requestType: RequestType.POST,
      sendObject: { login, password }
    });

};

export const clientLogout: IClientLogout = ({}) => {
  return dispatch =>
    dispatch({
      type: AT.CLIENT_LOGOUT
    });
};

export const setSession: ISetSession = ({ token }) => {
  return function(dispatch) {
    dispatch({
      type: AT.SET_SESSION,
      payload: {
        token
      }
    });
  };
};
