import Boom from "@hapi/boom";
// interfaces
import { ServerRoute } from "@hapi/hapi";
// new routes
import UserRoutes from "../helper/User/routes";
import AuthRoutes from "../helper/Auth/routes";
import TrainingRoutes from "../helper/Training/routes";
import { server } from "../server";
// import utils from "../utils";
// import { IDecoratedRequest } from "core/Api/interfaces";

const Routes: ServerRoute[] = [
  {
    method: "GET",
    path: "/hello",
    handler: () => {
      return "hello world";
    },
    options: {
      auth: false
    }
  },
  ...AuthRoutes,
  ...UserRoutes,
  ...TrainingRoutes
];

export default Routes;
