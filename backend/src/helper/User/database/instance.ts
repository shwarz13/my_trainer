import Sequelize from "sequelize";
import { IUserInstance, IUser, IUserBase } from "../interfaces";
import UserSchema from "./models";
import AuthToolkit from "../../../utils/AuthToolkit";
import { BASE_INSTANCE_PARAMS } from "../../../database/constants";

export default (sequelize: Sequelize.Sequelize) => {
  const instance = sequelize.define<IUserInstance, IUser, IUserBase>(
    "user",
    UserSchema,
    {
      ...BASE_INSTANCE_PARAMS,
      hooks: {
        beforeCreate: user => {
          user.password = AuthToolkit.getHash(user.password);
        },
        beforeUpdate: user => {
          user.password = AuthToolkit.getHash(user.password);
        }
      }
    }
  );

  return instance;
};
