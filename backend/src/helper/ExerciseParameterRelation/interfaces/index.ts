import { IBaseInstanceFields } from "../../../interfaces";
import Sequelize from "sequelize";

/* *
 *   ExerciseParameterRelation
 * */

export interface IExerciseParameterRelationBase {
  exercise: string;
  parameter: string;
  required: boolean;
}

export interface IExerciseParameterRelation
  extends IExerciseParameterRelationBase,
    IBaseInstanceFields {}

export type IExerciseParameterRelationInstance = Sequelize.Instance<
  IExerciseParameterRelation
> &
  IExerciseParameterRelation;
