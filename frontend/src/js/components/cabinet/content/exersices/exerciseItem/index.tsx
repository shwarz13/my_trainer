import React, {Fragment} from 'react';
import { hot } from 'react-hot-loader';
import { connect } from 'react-redux';
import Avatar from 'Components/baseComponents/Avatar';

// styles
import './index.scss';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';

interface IProps {
	id: string;
	img?: string;
	name: string;
	createdAt?: string;
}

// const path = require('Constants').initialPath;

const ExersiceItem = (props: IProps) => {

	const {id,img,name,createdAt} = props;

	return (
		<Fragment>
			<div className={'content-exercises__list__exercise-item'} >
				<div className={'content-exercises__list__exercise-item__wrapper-avatar'}>
					<Avatar alt={'У'} src={img} size={'large'} marginRight={'0px'}/>
				</div>
				<div className="content-exercises__list__exercise-item__info">{name}</div>
				<ChevronRightIcon/>
			</div>
		</Fragment>
	);
};


export default hot(module)(
	connect(
		null,
		null
	)(ExersiceItem)
);
