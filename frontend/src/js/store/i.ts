import { Dispatch, Store } from 'redux';

import { IAuthState } from 'Store/auth/i';
import { IRegisterState } from 'Store/registration/i';


export interface IState {
    loaded: ILoadingState;
    auth: IAuthState;
    register: IRegisterState;
}
export interface IRequestCreatorAction {
  type: string;
  requestType: string;
  requestUrl: string;
  resultField?: string;
  headers?: { [key: string]: string };
  sendObject?: { [key: string]: any };
  sendParams?: { [key: string]: any };
  other?: { [key: string]: any };
}

export type IAction<P, R> = (
  obj: P
) => (dispath: Dispatch, getState?: Store<IState>['getState']) => R;
export type IAsyncAction<P, R> = (
  obj: P
) => Promise<(dispath: Dispatch, getState?: Store<IState>['getState']) => R>;

export interface ILoadingState {
  [actionType: string]: LoadingStatus | IObjectsStatus | undefined;
}
export enum LoadingStatus {
  NONE,
  REQUEST,
  SUCCESS,
  FAIL,
}

interface IObjectsStatus {
  objects?: {};
}

