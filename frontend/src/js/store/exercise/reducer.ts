import { AnyAction } from 'redux';
import { IExercisesState, ActionTypes as AT } from './i';
import { RequestState } from 'Constants';


const initialState: IExercisesState = {
  exercises: [],
  images: [],
  currentExercise: {}
};

export default (state = initialState, action: AnyAction) => {
  switch (action.type) {
    case AT.GET_EXERCISES + RequestState.SUCCESS: {
      return {
        ...state,
        exercises: action.payload.results
      };
    }
    case AT.GET_IMAGES + RequestState.SUCCESS: {
      return {
        ...state,
        images: action.payload.results
      };
    }
    case AT.GET_ONE_EXERCISE + RequestState.SUCCESS: {
      return {
        ...state,
        currentExercise: action.payload
      };
    }
    case AT.CREATE_EXERCISE + RequestState.SUCCESS: {
      return {
        ...state
      };
    }
    default: {
      return state;
    }
  }
};
