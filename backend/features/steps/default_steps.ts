import { Given, Then, AfterAll, BeforeAll, setDefaultTimeout } from "cucumber";
import { server } from "../../src/server";
import { expect } from "chai";
import { getResponse } from "./lib/response";

import uuid = require("uuid");

import moment = require("moment");

let serverStarted = false;

setDefaultTimeout(25 * 1000);

Given("сервер стартовал", async function() {
  expect(serverStarted).to.eql(true);
});

Given("база данных пуста", async function() {
  await server.User.truncate({ cascade: true });
});

Then("сервер должен вернуть статус {int}", async function(int) {
  const lastResponse = getResponse();
  expect(lastResponse.statusCode).to.eql(int);
});

AfterAll(async function() {
  await server.stop();
});

BeforeAll(async function() {
  if (!serverStarted) {
    await server.start();
    serverStarted = true;
  }
});
