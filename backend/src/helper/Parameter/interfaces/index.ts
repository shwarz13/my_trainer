import { IBaseInstanceFields } from "../../../interfaces";
import Sequelize from "sequelize";
import { IMeasureMeta } from "../../Measure/interfaces";

/* *
 *   Parameter
 * */

export interface IParameterBase {
  name: string;
  description?: string | null;
  measureRequired: boolean;
}

export interface IParameter extends IParameterBase, IBaseInstanceFields {}

export type IParameterInstance = Sequelize.Instance<IParameter> & IParameter;

export interface IParameterMeta extends IParameterBase {
  id: string;
  measures: IMeasureMeta[];
  required: boolean;
}
