import { IAction } from '../i';

// state
export interface IRegisterState {
    clientRegister: boolean;
    statusCodeFail: boolean;
    statusCodeSuccess: boolean;
}

// actions
export interface IRegisterActions {
    clientRegister: IClientRegister;
    getCode?: IGetCode;
}
export enum ActionTypes {
    CLIENT_REGISTER = 'CLIENT_REGISTER',
    GET_CODE = 'GET_CODE'
}

// other
export type IClientRegister = IAction<{ email: string; password: string, firstName: string, lastName: string, phone:string }, any>;
export type IGetCode = IAction<{ email }, any>;
