import { AnyAction } from 'redux';
import { IAuthState, ActionTypes as AT } from './i';
import { RequestState } from 'Constants';
import Cookies from 'universal-cookie';


const initialState: IAuthState = {
  loggingIn: false,
  loggedIn: false
};
const cookies = new Cookies();

export default (state = initialState, action: AnyAction) => {
  switch (action.type) {
    case AT.CLIENT_LOGIN + RequestState.SUCCESS: {
      const { token, refreshToken, expiresIn } = action.payload;
      cookies.set('token', token);
      if (typeof cookies.get('refreshToken') === 'undefined') {
        cookies.set('refreshToken', refreshToken);
      }
      if (typeof cookies.get('expiresIn') === 'undefined') {
        cookies.set('expiresIn', expiresIn);
      }
      return {
        ...state,
        loggedIn: true,
        loggingIn: false
      };
    }
    case AT.CLIENT_LOGOUT: {
      cookies.remove('token', { path: '/' });
      cookies.remove('refreshToken', { path: '/' });
      cookies.remove('expiresIn', { path: '/' });

      return {
        ...state,
        loggedIn: false,
        loggingIn: false

      };
    }
    case AT.SET_SESSION: {
      const token = cookies.get('token');
      return { ...state, loggedIn: true, loggingIn: false, error: '' };
    }
    default: {
      return state;
    }
  }
};
