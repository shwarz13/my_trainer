import { IAction } from '../i';

// state
export interface IHeaderState {
  headerTitle?: any;

  leftBtn?: {
    isShow?: boolean;
    text?: string;
    func?: any;
    component?: any;
  }

  rightBtn?: {
    isShow?: boolean;
    text?: string;
    func?: any;
    component?: any;
  }
}

// actions
export interface IHeaderActions {
  headerTitle?: IHeaderTitle;
  setLeftBtn?: ISetLeftBtn;
  setRightBtn?: ISetRightBtn;
}

export enum ActionTypes {
  HEADER_TITLE = 'HEADER_TITLE',
  SET_LEFT_BTN = 'SET_LEFT_BTN',
  SET_RIGHT_BTN = 'SET_RIGHT_BTN'
}

// other
export type IHeaderTitle = IAction<{ title?: string }, any>;
export type ISetLeftBtn = IAction<{ isShow?: boolean, component?: any, text?: string, func?: any }, any>;
export type ISetRightBtn = IAction<{ isShow?: boolean, component?: any, text?: string, func?: any }, any>;

