import { API_VERSION } from "./env";

export const MAIN_URI = `/api/${API_VERSION}`;

export enum HTTP_METHODS {
  GET = "GET",
  POST = "POST",
  DEL = "DELETE",
  PUT = "PUT"
}

export enum HTTP_ERROR_CODES {
  BAD_REQUEST = 400,
  UNAUTHORIZED = 401,
  FORBIDDEN = 403,
  NOT_FOUND = 404
}

export const MIN_PASSWORD_LENGTH = 8;
export const REFRESH_TOKEN_EXPIRATION = 60 * 60 * 24 * 7;
