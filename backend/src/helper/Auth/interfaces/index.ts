export interface IAuthPayload {
  login: string;
  password: string;
}

export interface IAuthCredentials {
  id: string;
}

export interface IRefreshPayload {
  token: string;
  refreshToken: string;
}

export interface ILoginResponse {
  token: string;
  refreshToken: string;
  expiresIn: number;
}

export interface IResetPasswordPayload {
  oldPassword: string;
  newPassword: string;
}

export interface IResetPasswordCredentials {
  id: string;
  resetToken: string;
}
