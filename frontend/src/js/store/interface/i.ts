import { IAction } from '../i';
import { showSpinner } from 'Store/interface/actions';

// state
export interface IMainInterfaceState {
  isSpinnerShow?: boolean
}

// actions
export interface IMainInterfaceActions {
  showSpinner?: IShowSpinner;
}

export enum ActionTypes {
  SHOW_SPINNER = 'SHOW_SPINNER'
}

// other
export type IShowSpinner = IAction<{ show: boolean }, any>;

