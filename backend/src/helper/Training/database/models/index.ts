import Sequelize from "sequelize";
import { BASE_INSTANCE_FIELDS } from "../../../../database/constants";

const schema = {
  ...BASE_INSTANCE_FIELDS,
  date: {
    type: Sequelize.DATE,
    allowNull: false
  },
  place: {
    type: Sequelize.STRING(150)
  },
  trainer: {
    type: Sequelize.UUID
  },
  sportsman: {
    type: Sequelize.UUID,
    allowNull: false
  },
  name: {
    type: Sequelize.STRING(150)
  },
  comment: {
    type: Sequelize.STRING(255)
  }
};

export default schema;
