import Sequelize from "sequelize";
import uuid = require("uuid");

export const BASE_INSTANCE_FIELDS: Sequelize.DefineAttributes = {
  id: {
    type: Sequelize.UUID,
    unique: true,
    primaryKey: true,
    defaultValue: uuid.v4
  },
  createdAt: {
    type: Sequelize.NOW,
    field: "created_at",
    allowNull: false,
    defaultValue: Sequelize.literal
      ? Sequelize.literal("CURRENT_TIMESTAMP")
      : Sequelize.NOW
  },
  updatedAt: {
    type: Sequelize.NOW,
    field: "updated_at",
    allowNull: false,
    defaultValue: Sequelize.literal
      ? Sequelize.literal("CURRENT_TIMESTAMP")
      : Sequelize.NOW
  },
  deletedAt: {
    type: Sequelize.DATE,
    field: "deleted_at",
    defaultValue: null
  }
};

export const BASE_INSTANCE_PARAMS = {
  timestamps: true,
  paranoid: true,
  underscored: true,
  underscoredAll: true,
  createdAt: "created_at",
  updatedAt: "updated_at",
  deletedAt: "deleted_at"
};

export const DEFAULT_FOREIGN_PARAMS = {
  constraints: true,
  onDelete: "CASCADE",
  onUpdate: "CASCADE"
};
