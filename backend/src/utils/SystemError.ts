import { HTTP_ERROR_CODES } from "../constants";

export default class SystemError extends Error {
  public _code: number;
  public _message: string;

  constructor(obj = { message: "", code: HTTP_ERROR_CODES.BAD_REQUEST }) {
    super();
    this._message = obj.message;
    this._code = obj.code;
  }

  public static isSystemError(error) {
    return (
      error.message &&
      typeof error.message === "string" &&
      error.code &&
      typeof error.code === "number"
    );
  }

  get message() {
    return this._message;
  }

  get code() {
    return this._code;
  }
}
