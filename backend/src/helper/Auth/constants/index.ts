export const routeName = "auth";

export enum pathParams {
  LOGIN = "login",
  RESET_PASS = "reset-password",
  REFRESH = "refresh",
  REGISTRATION = "registration",
  FORGOTTEN = "forgotten",
  COMPLETE = "complete"
}
