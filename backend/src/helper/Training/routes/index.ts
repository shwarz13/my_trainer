import { MAIN_URI } from "../../../constants/index";
import Boom from "@hapi/boom";
// interfaces
import { ServerRoute } from "@hapi/hapi";
// controllers
import userCtrl from "../controllers";
// schemas

import { meta, list, newTraining, getItem, completeTraining } from "../docs/";
import { routeName, routes, pathParams } from "../constants";
import Joi from "@hapi/joi";

const Routes: ServerRoute[] = [
  {
    method: "GET",
    path: `${MAIN_URI}/${routeName}/${routes.META}`,
    handler: userCtrl.getMeta,
    options: {
      ...meta
    }
  },
  {
    method: "GET",
    path: `${MAIN_URI}/${routeName}`,
    handler: userCtrl.getList,
    options: {
      ...list
    }
  },
  {
    method: "GET",
    path: `${MAIN_URI}/${routeName}/{id}`,
    handler: userCtrl.getItem,
    options: {
      ...getItem
      // TODO: Допилить валадитор эндпоинта получения конкретной тренировки
      // validate: {
      //   params: {
      //     id: Joi.string()
      //       .uuid()
      //       .required(),
      //   },
      // },
    }
  },
  {
    method: "POST",
    path: `${MAIN_URI}/${routeName}`,
    handler: userCtrl.create,
    options: {
      ...newTraining,
      validate: {
        payload: Joi.object({
          date: Joi.date().required(),
          place: Joi.string().required(),
          sportsman: Joi.string()
            .uuid()
            .allow(null),
          name: Joi.string().required(),
          comment: Joi.string().allow(null),
          tasks: Joi.array().items(
            Joi.object({
              exercise: Joi.string()
                .uuid()
                .required(),
              comment: Joi.string().allow(null),
              sets: Joi.array().items(
                Joi.object({
                  order: Joi.number().required(),
                  setParameters: Joi.array().items(
                    Joi.object({
                      parameter: Joi.string()
                        .uuid()
                        .required(),
                      measure: Joi.string()
                        .uuid()
                        .allow(null),
                      plan: Joi.number().required()
                    })
                  )
                })
              )
            })
          )
        }).required()
      }
    }
  },
  {
    method: "POST",
    path: `${MAIN_URI}/${routeName}/{id}/${pathParams.COMPLETE}`,
    handler: userCtrl.complete,
    options: {
      ...completeTraining,
      validate: {
        payload: Joi.array()
          .items(
            Joi.object({
              setParameterRelationId: Joi.string()
                .uuid()
                .required(),
              fact: Joi.number().required()
            }).required()
          )
          .required()
        // TODO: Допилить валадитор эндпоинта получения конкретной тренировки
        // validate: {
        //   params: {
        //     id: Joi.string()
        //       .uuid()
        //       .required(),
        //   },
        // },
      }
    }
  }
];

export default Routes;
