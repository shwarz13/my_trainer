import { MAIN_URI, HTTP_METHODS } from "../../../constants/index";
import Boom from "@hapi/boom";
// interfaces
import { ServerRoute } from "@hapi/hapi";
// controllers
import userCtrl from "../controllers";

import { about, update } from "../docs/";
import { routeName, pathParams } from "../constants";
import Joi from "@hapi/joi";

const Routes: ServerRoute[] = [
  {
    method: HTTP_METHODS.GET,
    path: `${MAIN_URI}/${routeName}/${pathParams.ABOUT}`,
    handler: userCtrl.about,
    options: {
      ...about
    }
  },
  {
    method: HTTP_METHODS.PUT,
    path: `${MAIN_URI}/${routeName}`,
    handler: userCtrl.update,
    options: {
      ...update,
      validate: {
        payload: Joi.object({
          firstName: Joi.string()
            .optional()
            .min(2),
          lastName: Joi.string()
            .optional()
            .min(2),
          phone: Joi.string()
            .optional()
            .description("Номер телефона указывать без +7 и без 8")
            .allow(null),
          email: Joi.string()
            .optional()
            .email()
        })
      }
    }
  }
];

export default Routes;
