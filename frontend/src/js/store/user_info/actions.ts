import {
  IGetUserInfo,
  IUpdateUserInfo,
  ActionTypes as AT
} from './i';
import { RequestType } from 'Constants';
import {MAIN_URL} from 'Constants';

import { requestCreator } from '../request-creator';

const userId = Math.floor(Math.random() * 11) + 1;

export const getUserInfo: IGetUserInfo = ({}) => {
  return dispatch =>
    requestCreator(dispatch, {
      type: AT.GET_USER_INFO,
      requestUrl: `${MAIN_URL}user/about`,
      requestType: RequestType.GET
    })
};

export const updateUserInfo: IUpdateUserInfo = ({ firstName, lastName, phone, email }) => {
  return dispatch =>
    requestCreator(dispatch, {
      type: AT.UPDATE_USER_INFO,
      requestUrl: `${MAIN_URL}user`,
      requestType: RequestType.PUT,
      sendObject: { firstName, lastName, phone, email },
      other: { firstName, lastName, phone, email }
    })
};

export const changePassword = ({password, newPassword}) => {
  return dispatch =>
    requestCreator(dispatch, {
      type: AT.CHANGE_PASSWORD,
      requestUrl: `${MAIN_URL}changePassword`,
      requestType: RequestType.POST,
      sendObject: { password, newPassword }
    })
};

export const resetPasswordStatus = ({}) => {
  return dispatch =>
    dispatch({
      type: AT.RESET_PASSWORD
    })
};
