import Boom from "@hapi/boom";

import { IDecoratedRequest } from "../../../interfaces/index";

import methods from "../";
import { routeName, ErrorMessages } from "../constants";
import trycatcher from "../../../utils/trycatcher";
import { ISportMeta } from "../../Sport/interfaces";
import { IAuthCredentials } from "../../Auth/interfaces";
import { INewTraining, INewTrainingRequest } from "../interfaces";
import SystemError from "../../../utils/SystemError";
import uuid from "uuid";

const ctrl = {
  getMeta: trycatcher(
    async (req: IDecoratedRequest, h): Promise<ISportMeta[]> => {
      return methods.getMeta();
    },
    { isRequest: true, logMessage: `${routeName} getMeta` }
  ),
  getList: trycatcher(
    async (
      req: IDecoratedRequest<{}, {}, {}, IAuthCredentials>,
      h
    ): Promise<any> => {
      const user = req.auth.credentials;
      const filterParams = {};
      return methods.getList(user.id, filterParams);
    },
    { isRequest: true, logMessage: `${routeName} getList` }
  ),
  getItem: trycatcher(
    async (
      req: IDecoratedRequest<{}, {}, { id: string }, IAuthCredentials>,
      h
    ): Promise<any> => {
      const user = req.auth.credentials as IAuthCredentials;
      return methods.getItem(req.params.id, user.id);
    },
    { isRequest: true, logMessage: `${routeName} getItem` }
  ),
  create: trycatcher(
    async (
      req: IDecoratedRequest<INewTrainingRequest, {}, {}, IAuthCredentials>,
      h
    ): Promise<any> => {
      const training: INewTrainingRequest = req.payload;
      const { id: trainerId } = req.auth.credentials;
      return methods.create({
        ...training,
        trainer: trainerId,
        sportsman: training.sportsman || trainerId
      });
    },
    { isRequest: true, logMessage: `${routeName} create` }
  ),
  complete: trycatcher(
    async (
      req: IDecoratedRequest<any[], {}, { id: string }, IAuthCredentials>,
      h
    ): Promise<any> => {
      const user = req.auth.credentials as IAuthCredentials;
      return methods.complete(req.params.id, user.id, req.payload);
    },
    { isRequest: true, logMessage: `${routeName} complete` }
  )
};
export default ctrl;
