import { IBaseInstanceFields } from "../../../interfaces";
import Sequelize from "sequelize";
import { INewTask } from "../../Task/interfaces";

/* *
 *   Training
 * */

export interface ITrainingBase {
  date: string;
  place?: string;
  trainer: string;
  sportsman: string;
  name?: string;
  comment?: string | null;
}

export interface ITraining extends ITrainingBase, IBaseInstanceFields {}

export type ITrainingInstance = Sequelize.Instance<ITraining> & ITraining;

export interface ITrainingFilters {
  date?: Date;
}

export interface INewTrainingRequest {
  date: string;
  place: string;
  sportsman?: string | null;
  name: string;
  comment?: string | null;
  tasks: INewTask[];
}

export interface INewTraining extends INewTrainingRequest {
  sportsman: string;
  trainer: string;
}

export interface ITrainingItem {
  id: string;
  createdAt: Date;
  date: Date;
  place: string;
  name: string;
  comment: string | null;
  tasks: Array<{
    id: string;
    comment: string | null;
    exerciseItem: {
      id: string;
      name: string;
    };
    sets: Array<{
      id: string;
      order: number;
      setParameterRelations: Array<{
        id: string;
        plan: number;
        fact: number | null;
        parameterItem: {
          id: string;
          name: string;
          description: string | null;
        };
        measureItem: {
          id: string;
          name: string;
          description: string | null;
        };
      }>;
    }>;
  }>;
}

export interface ICompletedSetParameterRelation {
  setParameterRelationId: string;
  fact: number;
}
