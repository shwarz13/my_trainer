import React, { useEffect } from 'react';
import { Link, NavLink } from 'react-router-dom';
import { hot } from 'react-hot-loader';
import { connect } from 'react-redux';

// store
import { headerTitle } from 'Store/header/actions';
import {IHeaderActions} from 'Store/header/i';

// styles
import './index.scss';

type IProps = {
	routes?: any[];
	text?: boolean;
	currentPath?: string;
	location?: any;
	setTitle?: any;
} & IHeaderActions

// tslint:disable-next-line:no-var-requires
const path = require('Constants').initialPath;

const Menu = ({ routes, currentPath, setTitle }: IProps) => {

	const checkActiveState = url => {
		const regex = new RegExp(`${url.substring(0, url.length - 3)}\\w{1,3}\\/*`);
		return regex.test(currentPath);
	};

	return (
		<nav className="menu">
			{routes.map(({ title, path, image, notifications }) => {
					return title.length ? (
						<div className="menu__item" key={title}>
							<NavLink
								to={path}
								className={'menu__item__link'}
								activeClassName='menu__item_active'
								isActive={() => checkActiveState(path)}
								onClick={()=>setTitle(title)}
							>
								{image}
								{title}
							</NavLink>
							<span className="menu__item__notifications">
								{
									notifications ?
										<span className={'menu__item__notifications__value'}>{notifications}</span>:
										null
								}
							</span>
						</div>
					) : null
				}
			)}
		</nav>
	);
};

export default hot(module)(
	connect(
		null,
		{ headerTitle }
	)(Menu)
);
