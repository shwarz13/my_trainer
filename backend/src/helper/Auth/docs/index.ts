import { routeName } from "../constants/";
import Joi = require("@hapi/joi");

const loginResponse = Joi.object({
  token: Joi.string().required(),
  refreshToken: Joi.string().required(),
  expiresIn: Joi.number().required()
});

export const login = {
  description: "Авторизация пользователя",
  tags: ["api", routeName],
  plugins: {
    "hapi-swagger": {
      validate: {}
    }
  },
  response: { schema: loginResponse }
};

export const refresh = {
  description: "Обновление токена пользователя",
  tags: ["api", routeName],
  plugins: {
    "hapi-swagger": {
      validate: {}
    }
  },
  response: { schema: loginResponse }
};

export const registration = {
  description: "Регистрация нового пользователя",
  tags: ["api", routeName],
  plugins: {
    "hapi-swagger": {
      validate: {}
    }
  },
  response: { schema: Joi.boolean().required() }
};

export const resetPassword = {
  description: "Обновление пароля текущего пользователя",
  tags: ["api", routeName],
  plugins: {
    "hapi-swagger": {
      validate: {},
      security: [{ Bearer: [] }]
    }
  },
  response: { schema: Joi.boolean().required() }
};

export const resetForgottenPassword = {
  description: "Сброс забытого пароля",
  tags: ["api", routeName],
  plugins: {
    "hapi-swagger": {
      validate: {}
    }
  },
  response: { schema: Joi.boolean().required() }
};

export const resetForgottenPasswordComplete = {
  description: "Завершение процедуры сброса забытого пароля",
  tags: ["api", routeName],
  plugins: {
    "hapi-swagger": {
      validate: {},
      security: [{ Bearer: [] }]
    }
  },
  response: { schema: Joi.boolean().required() }
};
