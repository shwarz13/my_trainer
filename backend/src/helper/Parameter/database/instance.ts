import Sequelize from "sequelize";
import { IParameterInstance, IParameter, IParameterBase } from "../interfaces";
import ParameterSchema from "./models";
import { BASE_INSTANCE_PARAMS } from "../../../database/constants";

export default (sequelize: Sequelize.Sequelize) => {
  const instance = sequelize.define<
    IParameterInstance,
    IParameter,
    IParameterBase
  >("parameter", ParameterSchema, {
    ...BASE_INSTANCE_PARAMS
  });

  return instance;
};
