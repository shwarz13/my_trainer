import React, { Fragment, useEffect } from 'react';
import { hot } from 'react-hot-loader';
import { connect } from 'react-redux';
import './index.scss';
import { setLeftBtn } from 'Store/header/actions';
import { IHeaderActions } from 'Store/header/i';

type IProps = {
	history?: any;
} & IHeaderActions;

const Statistics = (props: IProps) => {
	useEffect(() => { props.setLeftBtn({isShow: true, text: 'Назад', func: () => props.history.goBack()}); }, []);
	return (
		<Fragment>
			<p>Это страница Статистики</p>
		</Fragment>
	);
};

export default hot(module)(
	connect(
		null,
		{ setLeftBtn }
	)(Statistics)
);
