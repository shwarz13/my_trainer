import { HTTP_ERROR_CODES } from "../../../constants";
const errors = {
  userNotFound: {
    code: HTTP_ERROR_CODES.NOT_FOUND,
    message: "Пользователь не найден"
  },
  updateUserError: {
    code: HTTP_ERROR_CODES.BAD_REQUEST,
    message: "Не удалось обновить данные пользователя"
  }
};
export default errors;
