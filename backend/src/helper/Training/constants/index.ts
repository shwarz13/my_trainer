import { HTTP_ERROR_CODES } from "../../../constants";

export const routeName = "training";

export enum routes {
  META = "meta"
}

export const attrFind = {
  default: [],
  mobile: []
};

export const ErrorMessages = {
  TrainingNotFound: {
    code: HTTP_ERROR_CODES.NOT_FOUND,
    message: "Тренировка не найдена"
  },
  CantCompleteTheseTasks: {
    code: HTTP_ERROR_CODES.BAD_REQUEST,
    message:
      "Не удалось завершить упражнения в тренировке. Попробуйте снова либо обратитесь в поддержку."
  },
  TrainingNotIncludesThisTask: (id: string) => ({
    code: HTTP_ERROR_CODES.BAD_REQUEST,
    message: `Не удалось завершить упражнения в тренировке. Такой id (${id}) не существует в тренировке. Обратитесь в поддержку.`
  })
};

export enum pathParams {
  COMPLETE = "complete"
}
