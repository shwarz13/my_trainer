import React, { Fragment, useEffect } from 'react';
import { hot } from 'react-hot-loader';
import { connect } from 'react-redux';

// styles
import { setLeftBtn } from 'Store/header/actions';
import { IHeaderActions } from 'Store/header/i';

type IProps = {
	history?: any;
} & IHeaderActions;

const Trainer = (props: IProps) => {

	useEffect(() => { props.setLeftBtn({isShow: true, text: 'Назад', func: () => props.history.goBack()}); }, []);

	return (
		<Fragment>
			<p>Это страница Тренера</p>
		</Fragment>
	);
};


export default hot(module)(
	connect(
		null,
		{ setLeftBtn }
	)(Trainer)
);
