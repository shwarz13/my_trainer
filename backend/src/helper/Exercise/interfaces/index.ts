import { IBaseInstanceFields } from "../../../interfaces";
import Sequelize from "sequelize";
import { IParameterMeta } from "../../Parameter/interfaces";

/* *
 *   Exercise
 * */

export interface IExerciseBase {
  name: string;
  description?: string | null;
  sport: string;
  media?: string | null;
}

export interface IExercise extends IExerciseBase, IBaseInstanceFields {}

export type IExerciseInstance = Sequelize.Instance<IExercise> & IExercise;

export interface IExerciseMeta extends IExerciseBase {
  id: string;
  parameters: IParameterMeta[];
}
