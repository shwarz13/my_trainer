import Sequelize from "sequelize";
import {
  ISetParameterRelationInstance,
  ISetParameterRelation,
  ISetParameterRelationBase
} from "../interfaces";
import SetParameterRelationSchema from "./models";
import { BASE_INSTANCE_PARAMS } from "../../../database/constants";

export default (sequelize: Sequelize.Sequelize) => {
  const instance = sequelize.define<
    ISetParameterRelationInstance,
    ISetParameterRelation,
    ISetParameterRelationBase
  >("set_parameter_relation", SetParameterRelationSchema, {
    ...BASE_INSTANCE_PARAMS,
    indexes: [
      {
        fields: ["set", "parameter"],
        type: "UNIQUE",
        where: {
          [BASE_INSTANCE_PARAMS.deletedAt]: {
            [Sequelize.Op.eq]: null
          }
        }
      },
      {
        fields: ["set", "measure"],
        type: "UNIQUE",
        where: {
          [BASE_INSTANCE_PARAMS.deletedAt]: {
            [Sequelize.Op.eq]: null
          }
        }
      }
    ]
  });

  return instance;
};
