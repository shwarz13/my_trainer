import Boom from "@hapi/boom";
import * as Hapi from "@hapi/hapi";
import * as AuthBearer from "hapi-auth-bearer-token";
import HapiSwagger from "hapi-swagger";
import Sequelize from "sequelize";
import Inert from "@hapi/inert";
import Vision from "@hapi/vision";
import redis from "redis";
import Bluebird from "bluebird";
Bluebird.promisifyAll(redis);
import cp from "child_process";
// db
import db from "./database";
import dbConnect from "./database/connect";
// interfaces
import { IUser, IUserBase, IUserInstance } from "./helper/User/interfaces";
import {
  ITraining,
  ITrainingBase,
  ITrainingInstance,
} from "./helper/Training/interfaces";
import { ITask, ITaskBase, ITaskInstance } from "./helper/Task/interfaces";
import { ISet, ISetBase, ISetInstance } from "./helper/Set/interfaces";
import {
  ISetParameterRelation,
  ISetParameterRelationBase,
  ISetParameterRelationInstance,
} from "./helper/SetParameterRelation/interfaces";
import { ISport, ISportBase, ISportInstance } from "./helper/Sport/interfaces";
import {
  IExercise,
  IExerciseBase,
  IExerciseInstance,
} from "./helper/Exercise/interfaces";
import {
  IParameter,
  IParameterBase,
  IParameterInstance,
} from "./helper/Parameter/interfaces";
import {
  IMeasure,
  IMeasureBase,
  IMeasureInstance,
} from "./helper/Measure/interfaces";
import {
  IExerciseParameterRelation,
  IExerciseParameterRelationBase,
  IExerciseParameterRelationInstance,
} from "./helper/ExerciseParameterRelation/interfaces";
import {
  IParameterMeasureRelation,
  IParameterMeasureRelationBase,
  IParameterMeasureRelationInstance,
} from "./helper/ParameterMeasureRelation/interfaces";
// routes
import Routes from "./routes/index";
// strategies
import Auth from "./strategies/Auth";
// utils
import logger from "./utils/Logger";

import { API_VERSION, SERVER_PORT, REDIS_URL } from "./constants/env";
import { HTTP_ERROR_CODES } from "./constants";
import RedisService from "./utils/services/RedisService";
import umzug from "./umzug";
import Logger from "./utils/Logger";
import { IDecoratedRequest } from "./interfaces";

class Server {
  private _httpServerPort: number;
  private _dbConnect: Sequelize.Sequelize;
  private _RedisService;
  private swaggerOptions = {
    info: {
      title: "Trainer Api",
      version: API_VERSION,
      description:
        "ОБЯЗАТЕЛЬНО! Для эндпоинтов, требующх авторизацию, перед токеном поставить Bearer",
    },

    jsonPath: `/swagger.json`,
    swaggerUIPath: `/swaggerui/`,
    documentationPath: `/documentation`,
    cors: true,
    grouping: "tags",
    securityDefinitions: {
      Bearer: {
        type: "apiKey",
        name: "Authorization",
        scheme: "bearer",
        in: "header",
      },
    },
    tagsGroupingFilter: (tag: string) => tag !== "api",
  };
  private _User: Sequelize.Model<IUserInstance, IUser, IUserBase>;
  private _Training: Sequelize.Model<
    ITrainingInstance,
    ITraining,
    ITrainingBase
  >;
  private _Task: Sequelize.Model<ITaskInstance, ITask, ITaskBase>;
  private _Set: Sequelize.Model<ISetInstance, ISet, ISetBase>;
  private _SetParameterRelation: Sequelize.Model<
    ISetParameterRelationInstance,
    ISetParameterRelation,
    ISetParameterRelationBase
  >;
  private _Sport: Sequelize.Model<ISportInstance, ISport, ISportBase>;
  private _Exercise: Sequelize.Model<
    IExerciseInstance,
    IExercise,
    IExerciseBase
  >;
  private _Parameter: Sequelize.Model<
    IParameterInstance,
    IParameter,
    IParameterBase
  >;
  private _Measure: Sequelize.Model<IMeasureInstance, IMeasure, IMeasureBase>;
  private _ExerciseParameterRelation: Sequelize.Model<
    IExerciseParameterRelationInstance,
    IExerciseParameterRelation,
    IExerciseParameterRelationBase
  >;
  private _ParameterMeasureRelation: Sequelize.Model<
    IParameterMeasureRelationInstance,
    IParameterMeasureRelation,
    IParameterMeasureRelationBase
  >;

  public _server: Hapi.Server;

  constructor(props: { port: number; dbConnect: Sequelize.Sequelize }) {
    this._httpServerPort = props.port;
    this._dbConnect = props.dbConnect;
  }

  public async createServer() {
    try {
      process.on("unhandledRejection", (error) => {
        logger.error(`unhandledRejection -->  ${error}`);
        process.exit(1);
      });

      this._server = new Hapi.Server({
        port: this._httpServerPort,
        routes: {
          cors: true,
          validate: {
            failAction: (request, h, err) => {
              throw err;
            },
          },
        },
      });

      await this._server.register([
        AuthBearer,
        Inert,
        Vision,
        {
          plugin: HapiSwagger,
          options: this.swaggerOptions,
        },
      ]);

      this._server.ext("onRequest", (req: IDecoratedRequest, h) => {
        Logger.info(`[REQUEST] - ${req.path}`);
        return h.continue;
      });

      this._server.auth.strategy("user-auth-jwt", "bearer-access-token", {
        validate: Auth.UserJWTAuth,
      });

      this._server.auth.strategy("admin-auth-jwt", "bearer-access-token", {
        validate: Auth.AdminJWTAuth,
      });

      this._server.auth.strategy("reset-pass-jwt", "bearer-access-token", {
        validate: Auth.ResetPassJWTAuth,
      });

      this._server.auth.default("user-auth-jwt");

      this._server.route(Routes);

      const client = redis.createClient(REDIS_URL);
      client.on("error", function(err) {
        logger.error("[REDIS] Error: " + err);
      });
      this._RedisService = new RedisService(client);
    } catch (error) {
      logger.error(`Create server --> ${error}`);
    }
  }

  private async startMigrations(connect) {
    const instances = umzug(connect);
    for (const instanceKey in instances) {
      if ((await instances[instanceKey].pending()).length > 0) {
        logger.info("------------------");
        logger.info(`${instanceKey}s started`);

        await instances[instanceKey].up();

        logger.info(`${instanceKey}s finished`);
        logger.info("------------------");
      } else {
        logger.info(`List of ${instanceKey.toLowerCase()}s is empty`);
      }
    }
  }

  private async connectToDb() {
    try {
      await this._dbConnect.authenticate({ logging: false });
      logger.info("Success authenticate to DB");
      await this.defineAndsyncModel(this._dbConnect);
      logger.info("Success connect to DB");
      await this.startMigrations(this._dbConnect);
    } catch (error) {
      logger.error(`Error connect to DB --> ${error}`);
      process.exit(1);
    }
  }

  private async dbStop() {
    try {
      await this._dbConnect.close();
    } catch (error) {
      logger.error("DB Stop error", error);
    }
  }

  private async defineAndsyncModel(connect: Sequelize.Sequelize) {
    const response = db(connect);

    Object.keys(response).forEach((modelKey) => {
      this[`_${modelKey}`] = response[modelKey];
    });

    // TODO: приудмать нормальную обработку
    await this._dbConnect.sync({
      // force: true,
      // alter: true,
      logging: false,
    });

    return;
  }

  public generateHttpError(data) {
    const { code, message } = data;
    switch (code) {
      case 11000:
      case HTTP_ERROR_CODES.BAD_REQUEST:
        return Boom.badRequest(message);
      case HTTP_ERROR_CODES.NOT_FOUND:
        return Boom.notFound(message);
      case HTTP_ERROR_CODES.FORBIDDEN:
        return Boom.forbidden(message);
      case HTTP_ERROR_CODES.UNAUTHORIZED:
        return Boom.unauthorized(message);
      default:
        logger.error(data);
        return Boom.badImplementation();
    }
  }

  public async startServer() {
    await this._server.start();
    logger.info("Server running on: ", this._server.info.uri);
  }

  public async start() {
    await this.createServer();
    await this.startServer();
    await this.connectToDb();
  }

  private async serverStop() {
    await this._server.stop();
  }

  public async stop() {
    await this.serverStop();
    await this.dbStop();
  }

  public async reload() {
    await this.stop();
    await this.start();
  }

  get server() {
    return this._server;
  }
  get dbConnect() {
    return this._dbConnect;
  }
  get RedisService() {
    return this._RedisService;
  }

  // Entities getters

  get User() {
    return this._User;
  }
  get Training() {
    return this._Training;
  }
  get Task() {
    return this._Task;
  }
  get Set() {
    return this._Set;
  }
  get SetPerameterRelation() {
    return this._SetParameterRelation;
  }
  get Sport() {
    return this._Sport;
  }
  get Exercise() {
    return this._Exercise;
  }
  get Parameter() {
    return this._Parameter;
  }
  get Measure() {
    return this._Measure;
  }
  get ExerciseParameterRelation() {
    return this._ExerciseParameterRelation;
  }
  get ParameterMeasureRelation() {
    return this._ParameterMeasureRelation;
  }
}

export const server = new Server({
  port: SERVER_PORT,
  dbConnect,
});
