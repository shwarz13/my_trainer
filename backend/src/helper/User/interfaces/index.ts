import { IBaseInstanceFields } from "../../../interfaces";
import Sequelize from "sequelize";

/* *
 *   User
 * */

export interface IUserBase {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  phone?: string | null;
  socials?: object | null;
  avatar?: string | null;
}

export interface IUserJSON {
  id: string;
  phone?: string | null;
  email?: string | null;
  firstName: string;
  lastName: string;
  avatar?: string | null;
  socials?: object | null;
}

export interface ICreateUserPayload {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  phone: string;
}

export interface IUpdateUserPayload {
  firstName?: string | null;
  lastName?: string | null;
  email?: string | null;
  phone?: string | null;
  password?: string | null;
}

export interface IUser extends IUserBase, IBaseInstanceFields {}

export type IUserInstance = Sequelize.Model<IUser> & IUser;
