import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { hot } from 'react-hot-loader';
import { connect } from 'react-redux';

// store
import { IHeaderTitle } from 'Store/header/i';

// styles
import './index.scss';

type IProps = {
	routes?: any;
} & IHeaderTitle;

// tslint:disable-next-line:no-var-requires
const path = require('Constants').initialPath;

const Content = (props: IProps) => {

	const {routes} = props;

	return (
		<Switch>
			<Redirect exact from={`${path}lk`} to={`${path}lk/training`} />
			{routes.map(({ path, component: C, title }) => (
				<Route
					key={`route-${title}`}
					render={props => {
						return <C {...props} />;
					}}
					{...{ path }}
				/>
			))}
		</Switch>
	);
};

export default hot(module)(
	connect(
		null,
		null
	)(Content)
);
