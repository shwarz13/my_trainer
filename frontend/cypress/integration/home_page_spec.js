describe('The Home Page', function() {
    it('successfully loads', function() {
        cy.visit('/')
        cy.get('.auth-link').click();

        cy.get('input[name="name"]').type(`3`)
        cy.get('input[name="password"]').type(`4`)
        cy.get('button[type=submit]').click();

        cy.get('h1').should('contain', 'Проекты')

        // auth cookie should be present
        cy.getCookie('token').should('exist');
        cy.getCookie('refreshToken').should('exist');
        cy.getCookie('expiresIn').should('exist');

        cy.get(':nth-child(2) > .sidebar-nav-link').click();
        cy.get('[href="#/lk/projects/b5645f1e-427c-11e9-9a9b-3c77e6ef04ce/common"] > .object_item_content > .address').click();
        cy.get('.top-menu > :nth-child(3) > .sidebar-nav-link').click();
        cy.get(':nth-child(3) > .widget_item_link').click();
        cy.get(':nth-child(5) > .element_content > .element_text').click();
        cy.get('nav > :nth-child(2) > .sidebar-nav-link').click();
        cy.get('nav > :nth-child(1) > .sidebar-nav-link').click();
        cy.get('.page').children();
    })
});
