import { IBaseInstanceFields } from "../../../interfaces";
import Sequelize from "sequelize";

/* *
 *   SetParameterRelation
 * */

export interface ISetParameterRelationBase {
  set: string;
  parameter: string;
  measure?: string | null;
  plan: number;
  fact?: number | null;
}

export interface ISetParameterRelation
  extends ISetParameterRelationBase,
    IBaseInstanceFields {}

export type ISetParameterRelationInstance = Sequelize.Instance<
  ISetParameterRelation
> &
  ISetParameterRelation;
