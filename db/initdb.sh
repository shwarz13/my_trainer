#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    SELECT 'CREATE USER trainer_dbadmin' WHERE NOT EXISTS (SELECT FROM pg_catalog.pg_roles WHERE rolname = 'trainer_dbadmin');
    SELECT 'CREATE DATABASE trainer' WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = 'trainer');
    GRANT ALL PRIVILEGES ON DATABASE trainer TO trainer_dbadmin;
EOSQL