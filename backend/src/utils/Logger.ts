import winston from "winston";

const prettyObjects = winston.format.printf((data: any) => {
  let message;

  if (data.message) {
    if (
      typeof data.message === "object" &&
      [Array, Object].includes(data.message.constructor)
    ) {
      message = JSON.stringify(data.message, null, 4); // eslint-disable-line
    } else {
      message = data.message;
    }
  } else {
    message = JSON.stringify(data, null, 4); // eslint-disable-line
  }

  return `${new Date().toISOString()} ${data.level}: ${message}`;
});

/* используем CommonJs вместо imoport/export, чтобы можно было пользоваться логгером во всех файлах */
export default winston.createLogger({
  level: "debug",
  transports: [
    new winston.transports.Console({
      format: winston.format.combine(
        winston.format.colorize(),
        winston.format.simple(),
        prettyObjects
      )
    })
  ]
});
