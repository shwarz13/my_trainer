import Boom from "@hapi/boom";

import { IDecoratedRequest } from "../../../interfaces";

import { IUserJSON, IUpdateUserPayload } from "../interfaces";

import UserMethods from "../";
import { routeName } from "../constants";
import trycatcher from "../../../utils/trycatcher";
import SystemError from "../../../utils/SystemError";
import errors from "../constants/errors";
import { IAuthCredentials } from "../../Auth/interfaces";

const UserController = {
  about: trycatcher(
    async (
      req: IDecoratedRequest<{}, {}, {}, IAuthCredentials>,
      h
    ): Promise<IUserJSON> => {
      const credentials = req.auth.credentials;
      const userInstance = await UserMethods.findUserById(credentials.id);
      if (!userInstance) {
        throw new SystemError(errors.userNotFound);
      }
      return (userInstance.toJSON() as any) as IUserJSON;
    },
    { isRequest: true, logMessage: `${routeName} about` }
  ),
  update: trycatcher(
    async (
      req: IDecoratedRequest<IUpdateUserPayload, {}, {}, IAuthCredentials>,
      h
    ): Promise<IUserJSON> => {
      const newUserData = req.payload;
      const userId = req.auth.credentials.id;

      const result = await UserMethods.update(userId, newUserData);
      if (!result[0]) {
        throw new SystemError(errors.updateUserError);
      }
      const userInstance = result[1][0];
      return {
        id: userInstance.id,
        firstName: userInstance.first_name,
        lastName: userInstance.last_name,
        avatar: userInstance.avatar,
        socials: userInstance.socials,
        email: userInstance.email,
        phone: userInstance.phone,
      };
    },
    { isRequest: true, logMessage: `${routeName} about` }
  ),
};

export default UserController;
