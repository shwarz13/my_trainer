import { IBaseInstanceFields } from "../../../interfaces";
import Sequelize from "sequelize";
import { ISetParameterRelationBase } from "../../SetParameterRelation/interfaces";

/* *
 *   Set
 * */

export interface ISetBase {
  task: string;
  order: number;
}

export interface ISet extends ISetBase, IBaseInstanceFields {}

export type ISetInstance = Sequelize.Instance<ISet> & ISet;

export interface INewSet extends ISetBase {
  setParameters: ISetParameterRelationBase[];
}
