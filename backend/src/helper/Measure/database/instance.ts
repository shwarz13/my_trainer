import Sequelize from "sequelize";
import { IMeasureInstance, IMeasure, IMeasureBase } from "../interfaces";
import MeasureSchema from "./models";
import { BASE_INSTANCE_PARAMS } from "../../../database/constants";

export default (sequelize: Sequelize.Sequelize) => {
  const instance = sequelize.define<IMeasureInstance, IMeasure, IMeasureBase>(
    "measure",
    MeasureSchema,
    {
      ...BASE_INSTANCE_PARAMS
    }
  );

  return instance;
};
