import { requestCreator } from '../request-creator';
import { MAIN_URL } from 'Constants';

import {
  IClientRegister,
  ActionTypes as AT, IGetCode
} from './i';
import { RequestType } from 'Constants';

export const clientRegister: IClientRegister = ({ email, password, phone, firstName, lastName }) => {
  return dispatch =>
    requestCreator(dispatch, {
      type: AT.CLIENT_REGISTER,
      requestUrl: `${MAIN_URL}auth/registration`,
      requestType: RequestType.POST,
      sendObject: { email, password, phone, firstName, lastName}
    });
};

export const getCode:IGetCode = ({ email }) => {
  return dispatch =>
    requestCreator(dispatch, {
      type: AT.GET_CODE,
      requestUrl: `${MAIN_URL}sendCode`,
      requestType: RequestType.POST,
      sendObject: { email }
    });
};
