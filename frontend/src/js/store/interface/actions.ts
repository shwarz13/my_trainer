import {
  IShowSpinner,
  ActionTypes as AT
} from './i';

export const showSpinner: IShowSpinner = ({show}) => {
  return dispatch =>
    dispatch({
      type: AT.SHOW_SPINNER,
      other: { show }
  });
};
