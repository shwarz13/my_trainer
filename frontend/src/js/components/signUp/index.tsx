import React, { Component, useState } from 'react';
import { hot } from 'react-hot-loader';
import { connect } from 'react-redux';

import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { NavLink } from 'react-router-dom';
import { clientRegister, getCode } from 'Store/registration/actions';

function Copyright() {
	return (
		<Typography variant="body2" color="textSecondary" align="center">
			{'Copyright © '}
			<Link color="inherit" href="#">
				Your Train
			</Link>{' '}
			{new Date().getFullYear()}
			{'.'}
		</Typography>
	);
}

const path = require('Constants').initialPath;

const useStyles = makeStyles(theme => ({
	paper: {
		marginTop: theme.spacing(4),
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
	},
	form: {
		width: '100%', // Fix IE 11 issue.
		marginTop: theme.spacing(1),
	},
	submit: {
		margin: theme.spacing(3, 0, 2),
	},
}));

const SignUp = (props) => {
	const classes = useStyles();

	const [email,setEmail] = useState('');
	const [password,setPassword] = useState('');
	const [firstName,setFirstName] = useState('');
	const [lastName,setLastName] = useState('');

	const handleLogin = async e => {
		e.preventDefault();
		if(email && password && firstName && lastName) {
			await props.clientRegister({ email, password, firstName, lastName });
			props.history.push(`${path}lk`);
		}
	};

	return (
		<Container component="main" maxWidth="xs" className={'absolute_centre bg_light'}>
			<CssBaseline />
			<div className={classes.paper}>
				<Typography component="h1" variant="h5">
					Регистрация
				</Typography>
				<form onSubmit={handleLogin} className={classes.form} noValidate>
					<Grid container spacing={2}>
						<Grid item xs={12} sm={6}>
							<TextField
								autoComplete="fname"
								name="firstName"
								variant="outlined"
								required
								fullWidth
								id="firstName"
								label="Ваше имя"
								value={firstName}
								onChange={e => setFirstName(e.target.value)}
								autoFocus
							/>
						</Grid>
						<Grid item xs={12} sm={6}>
							<TextField
								variant="outlined"
								required
								fullWidth
								id="lastName"
								label="Ваша фамилия"
								name="lastName"
								autoComplete="lname"
								value={lastName}
								onChange={e => setLastName(e.target.value)}
							/>
						</Grid>
						<Grid item xs={12}>
							<TextField
								variant="outlined"
								required
								fullWidth
								id="email"
								label="Ваш e-mail"
								name="email"
								autoComplete="email"
								value={email}
								onChange={e => setEmail(e.target.value)}
							/>
						</Grid>
						<Grid item xs={12}>
							<TextField
								variant="outlined"
								required
								fullWidth
								name="password"
								label="Ваш пароль"
								type="password"
								id="password"
								autoComplete="current-password"
								value={password}
								onChange={e => setPassword(e.target.value)}
							/>
						</Grid>
						<Grid item xs={12}>
							<FormControlLabel
								control={<Checkbox value="allowExtraEmails" color="primary" />}
								label="Я хочу получать информацию об акциях и статьи о здоровье"
							/>
						</Grid>
					</Grid>
					<Button
						type="submit"
						fullWidth
						variant="contained"
						color="primary"
						className={classes.submit}
					>
						Зерегистрироваться
					</Button>
					<Grid container justify="flex-end">
						<Grid item>
							<NavLink to={`${path}sign_in`}>Уже есть аккаунт? Войдите</NavLink>
						</Grid>
					</Grid>
				</form>
			</div>
			<Box mt={5} mb={1}>
				<Copyright />
			</Box>
		</Container>
	);
};

const mapStateToProps = (state) => {
	return  {
		register: state.register.clientRegister,
		loaded: state.loaded.CLIENT_REGISTER,
		codeSuccess: state.register.statusCodeSuccess,
		codeFail: state.register.statusCodeFail
	}
};

const mapDispatchToProps = { clientRegister };

export default hot(module)(
	connect(
		mapStateToProps,
		mapDispatchToProps
	)(SignUp)
);
