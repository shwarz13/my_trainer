import { IBaseInstanceFields } from "../../../interfaces";
import Sequelize from "sequelize";

/* *
 *   ParameterMeasureRelation
 * */

export interface IParameterMeasureRelationBase {
  parameter: string;
  measure: string;
  // required: boolean;
}

export interface IParameterMeasureRelation
  extends IParameterMeasureRelationBase,
    IBaseInstanceFields {}

export type IParameterMeasureRelationInstance = Sequelize.Instance<
  IParameterMeasureRelation
> &
  IParameterMeasureRelation;
