import Sequelize from "sequelize";
import { BASE_INSTANCE_FIELDS } from "../../../../database/constants";

const schema = {
  ...BASE_INSTANCE_FIELDS,
  name: {
    type: Sequelize.STRING,
    allowNull: false
  },
  description: {
    type: Sequelize.STRING
  }
};

export default schema;
