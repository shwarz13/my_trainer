import { Given, When, Then } from "cypress-cucumber-preprocessor/steps";
import axios from 'axios';

const url = "https://esbunoto.lad24.ru/#/";

let random = null;

Given('я на главной странице', () => {
  cy.visit(url)
});

Given('я прихожу на сайт по ссылке с демо', () => {
  cy.visit(url + 'lk?demo=true');
  // cy.visit(url+'lk');
  // cy.get('input[name="name"]').type('3');
  // cy.get('input[name="password"]').type('4');
  // cy.get('button[type="submit"]').click();
});

When('я нажимаю {string}', (name) => {
  cy.contains(name).click();
});

Then('я вижу {string}', (name) => {
  cy.contains(name);
});

Then('я не вижу {string}', (name) => {
  cy.contains(name).should('not.exist');
});


When('я ввожу {string} в поле логин', (login) => {
  cy.get('input[name="name"]').type(login);
});
When('я ввожу {string} в поле пароль', (password) => {
  cy.get('input[name="password"]').type(password);
});

When('я нажимаю {string}', (name) => {
  cy.contains(name).click();
});

Then('я вижу {string}', (name) => {
  cy.contains(name);
});

When('я выбираю проект {string}', (name) => {
  cy.contains(name).parents('.project').find('.project_show').click();
});

When('я разворачиваю раздел {string}', (name) => {
  cy.contains(name).find('.element_open').click();
});

When('нажимаю перейти к работам компонента {string}', (name) => {
  cy.get(`.element_component:contains('${name}'):visible`).find('a:contains("Перейти к работам"):visible').click();
});

When('я добавляю через мобильное приложение случайный факт на компонент {string}', async (name) => {
  random = Math.floor(Math.random() * 10000);
  const jwt = cy.getCookie('token');
  cy.log('JWT');
  cy.log(jwt);
  const components = await axios.get('https://csp24.ru/esb/1.0/component',{
    headers: {
      'authorization': `Bearer ${jwt}`
    }
  });
  cy.log(JSON.stringify(components));
});


When('обновляю страницу', () => {
  cy.reload();
});
