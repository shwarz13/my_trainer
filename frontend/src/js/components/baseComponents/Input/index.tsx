import React, { useEffect, useState } from 'react';
import { hot } from 'react-hot-loader';
import { connect } from 'react-redux';
import { TextField } from '@material-ui/core';
// store


// images


// styles
import './index.scss';
import { makeStyles } from '@material-ui/core/styles';


type IProps = {
	type?: string;
	required?: boolean;
	disabled?: boolean;
	error?: boolean;
	label?: string;
	value?: string;
	placeholder?: string;
	onChange?: any;
	multiline?: boolean;
	rowsMax?: number;
	width?: string;
};

const path = require('Constants').initialPath;

const Input = (props: IProps) => {

	const {
		type='standart',
		multiline=false,
		rowsMax=1,
		required=false,
		disabled=false,
		label='',
		value='',
		error=false,
		placeholder='',
		onChange,
		width='100%'
	} = props;
	const [inputValue, setInputValue] = useState(value);

	const useStyles = makeStyles(theme => ({
		root: {
			color: '#000',
			width
		}
	}));

	useEffect(() => onChange(inputValue), [inputValue]);
	const classes = useStyles();
	return (
		<TextField
			{...label && {label}}
			value={inputValue}
			placeholder={placeholder}
			error={error}
			multiline={multiline}
			rowsMax={rowsMax}
			required={required}
			className={classes.root}
			disabled={disabled}
			onChange={e => setInputValue(e.target.value)}
		/>
	);
};


export default hot(module)(
	connect(
		null,
		null
	)(Input)
);
