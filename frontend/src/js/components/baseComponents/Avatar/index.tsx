import React, { useEffect, useState } from 'react';
import { hot } from 'react-hot-loader';
import { connect } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import { Avatar } from '@material-ui/core';
// store


// images


// styles
import './index.scss';


type IProps = {
	alt?: string;
	src?: string;
	size?: string;
	marginRight?: string;
};

const path = require('Constants').initialPath;

const AvatarComponent = (props: IProps) => {

	const useStyles = makeStyles(theme => ({
		root: {
			display: 'flex',
			'& > *': {
				margin: theme.spacing(1),
			},
		},
		small: {
			width: theme.spacing(3),
			height: theme.spacing(3),
		},
		large: {
			width: theme.spacing(10),
			height: theme.spacing(10),
			border: '1px solid white',
			marginRight: props.marginRight || '20px'
		},
	}));

	const { alt='Img', src='Поле ввода', size } = props;
	const classes = useStyles();

	return (
		<Avatar alt={alt} src={src} className={ size ? classes[size] : ''} />
	);
};


export default hot(module)(
	connect(
		null,
		null
	)(AvatarComponent)
);
