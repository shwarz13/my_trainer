import React, { Component } from 'react';
import { hot } from 'react-hot-loader';
import { connect } from 'react-redux';

// styles
import './index.scss';

type IProps = {
	status: string;
	message?: any;
}

class Message extends Component<IProps> {

	public render() {

		const {status, message} = this.props;
		return (
			<div className={'message ' + status}>
				{message}
			</div>
		);
	}
}


export default hot(module)(
	connect(
		null,
		null
	)
	(Message));
