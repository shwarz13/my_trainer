import Sequelize from "sequelize";

import UserInstance from "../helper/User/database/instance";
import TrainingInstance from "../helper/Training/database/instance";
import TaskInstance from "../helper/Task/database/instance";
import SetInstance from "../helper/Set/database/instance";
import SetParameterRelationInstance from "../helper/SetParameterRelation/database/instance";
import SportInstance from "../helper/Sport/database/instance";
import ExerciseInstance from "../helper/Exercise/database/instance";
import ParameterInstance from "../helper/Parameter/database/instance";
import MeasureInstance from "../helper/Measure/database/instance";
import ExerciseParameterRelationInstance from "../helper/ExerciseParameterRelation/database/instance";
import ParameterMeasureRelationInstance from "../helper/ParameterMeasureRelation/database/instance";
import { DEFAULT_FOREIGN_PARAMS } from "./constants";

export default (sequelize: Sequelize.Sequelize) => {
  const models = {
    User: UserInstance(sequelize),
    Training: TrainingInstance(sequelize),
    Task: TaskInstance(sequelize),
    Set: SetInstance(sequelize),
    SetParameterRelation: SetParameterRelationInstance(sequelize),
    Sport: SportInstance(sequelize),
    Exercise: ExerciseInstance(sequelize),
    Parameter: ParameterInstance(sequelize),
    Measure: MeasureInstance(sequelize),
    ExerciseParameterRelation: ExerciseParameterRelationInstance(sequelize),
    ParameterMeasureRelation: ParameterMeasureRelationInstance(sequelize)
  };

  // ASSOCIATIONS
  // EXERCISE
  models.Exercise.hasMany(models.Task, {
    foreignKey: "exercise",
    ...DEFAULT_FOREIGN_PARAMS,
    onDelete: "NO ACTION"
  });
  models.Exercise.belongsToMany(models.Parameter, {
    through: models.ExerciseParameterRelation,
    ...DEFAULT_FOREIGN_PARAMS,
    foreignKey: "exercise",
    otherKey: "parameter"
  });
  models.Exercise.belongsTo(models.Sport, {
    foreignKey: "sport",
    as: "sportItem",
    ...DEFAULT_FOREIGN_PARAMS
  });
  // MEASURE
  models.Measure.belongsToMany(models.Parameter, {
    through: models.ParameterMeasureRelation,
    ...DEFAULT_FOREIGN_PARAMS,
    foreignKey: "measure",
    otherKey: "parameter"
  });
  models.Measure.hasMany(models.SetParameterRelation, {
    ...DEFAULT_FOREIGN_PARAMS,
    foreignKey: "measure"
  });
  // PARAMETER
  models.Parameter.belongsToMany(models.Exercise, {
    through: models.ExerciseParameterRelation,
    ...DEFAULT_FOREIGN_PARAMS,
    foreignKey: "parameter",
    otherKey: "exercise"
  });
  models.Parameter.belongsToMany(models.Measure, {
    through: models.ParameterMeasureRelation,
    ...DEFAULT_FOREIGN_PARAMS,
    foreignKey: "parameter",
    otherKey: "measure"
  });
  models.Parameter.hasMany(models.SetParameterRelation, {
    ...DEFAULT_FOREIGN_PARAMS,
    foreignKey: "paramter"
  });
  // SET PARAMETER RELATIONS
  models.SetParameterRelation.belongsTo(models.Parameter, {
    ...DEFAULT_FOREIGN_PARAMS,
    foreignKey: "parameter",
    as: "parameterItem"
  });
  models.SetParameterRelation.belongsTo(models.Measure, {
    ...DEFAULT_FOREIGN_PARAMS,
    foreignKey: "measure",
    as: "measureItem"
  });
  models.SetParameterRelation.belongsTo(models.Set, {
    ...DEFAULT_FOREIGN_PARAMS,
    foreignKey: "set",
    as: "setItem"
  });
  // SET
  models.Set.hasMany(models.SetParameterRelation, {
    ...DEFAULT_FOREIGN_PARAMS,
    foreignKey: "set",
    as: "setParameterRelations"
  });
  models.Set.belongsTo(models.Task, {
    as: "taskItem",
    foreignKey: "task",
    ...DEFAULT_FOREIGN_PARAMS
  });
  // SPORT
  models.Sport.hasMany(models.Exercise, {
    foreignKey: "sport",
    ...DEFAULT_FOREIGN_PARAMS,
    onDelete: "NO ACTION"
  });
  // TASK
  models.Task.belongsTo(models.Exercise, {
    as: "exerciseItem",
    foreignKey: "exercise",
    ...DEFAULT_FOREIGN_PARAMS
  });
  models.Task.hasMany(models.Set, {
    foreignKey: "task",
    ...DEFAULT_FOREIGN_PARAMS
  });
  models.Task.belongsTo(models.Training, {
    as: "trainingItem",
    foreignKey: "training",
    ...DEFAULT_FOREIGN_PARAMS
  });
  // TRAINING
  models.Training.hasMany(models.Task, {
    foreignKey: "training",
    ...DEFAULT_FOREIGN_PARAMS
  });
  models.Training.belongsTo(models.User, {
    as: "trainerPerson",
    foreignKey: "trainer",
    ...DEFAULT_FOREIGN_PARAMS
  });
  models.Training.belongsTo(models.User, {
    as: "sportsmanPerson",
    foreignKey: "sportsman",
    ...DEFAULT_FOREIGN_PARAMS
  });
  // USER
  models.User.hasMany(models.Training, {
    foreignKey: "trainer",
    ...DEFAULT_FOREIGN_PARAMS
  });
  models.User.hasMany(models.Training, {
    foreignKey: "sportsman",
    ...DEFAULT_FOREIGN_PARAMS
  });

  return models;
};
