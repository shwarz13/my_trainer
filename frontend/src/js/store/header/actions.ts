import {
  IHeaderTitle, ISetLeftBtn, ISetRightBtn,
  ActionTypes as AT
} from './i';

export const headerTitle: IHeaderTitle = ({title})  => {
  return dispatch =>
    dispatch({
      type: AT.HEADER_TITLE,
      other: { title }
  });
};

export const setLeftBtn: ISetLeftBtn = ({isShow, text, func, component})  => {
  return dispatch =>
    dispatch({
      type: AT.SET_LEFT_BTN,
      payload: { isShow, text, func, component }
    });
};

export const setRightBtn: ISetRightBtn = ({isShow, text, func, component})  => {
  return dispatch =>
    dispatch({
      type: AT.SET_RIGHT_BTN,
      payload: { isShow, text, func, component }
    });
};
