import { MAIN_URI } from "../../../constants/index";
import Boom from "@hapi/boom";
// interfaces
import { ServerRoute } from "@hapi/hapi";
// controllers
import ctrl from "../controllers";
// schemas

// import { read } from "../docs/";
import { routeName } from "../constants";
import Joi from "@hapi/joi";

const Routes: ServerRoute[] = [
  // {
  //   method: "GET",
  //   path: `${MAIN_URI}/${routeName}`,
  //   handler: ctrl.read,
  //   options: {
  //     ...read,
  //     validate: {
  //       query: Joi.object({
  //         date: Joi.date().required(),
  //         place: Joi.string().min(2),
  //         additional: Joi.string()
  //       })
  //     }
  //   }
  // }
];

export default Routes;
