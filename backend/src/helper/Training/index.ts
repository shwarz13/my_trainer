import logger from "../../utils/Logger";

import { Op } from "sequelize";

import { server } from "../../server";
import { ISportMeta, ISportInstance } from "../Sport/interfaces";
import { IExerciseInstance, IExerciseMeta } from "../Exercise/interfaces";
import { IParameterInstance, IParameterMeta } from "../Parameter/interfaces";
import { IMeasureInstance, IMeasureMeta } from "../Measure/interfaces";
import {
  ITraining,
  ITrainingFilters,
  INewTraining,
  ITrainingItem,
  ICompletedSetParameterRelation
} from "./interfaces";
import TaskMethods from "../Task";
import SystemError from "../../utils/SystemError";
import { ErrorMessages } from "./constants";

interface ISportInstanceWithExercises extends ISportInstance {
  exercises: Array<
    IExerciseInstance & {
      parameters: Array<
        IParameterInstance & {
          measures: IMeasureInstance[];
          exercise_parameter_relation: {
            id: string;
            required: boolean;
          };
        }
      >;
    }
  >;
}

class Methods {
  public static async getMeta() {
    return ((await server.Sport.findAll({
      attributes: ["id", "name", "description"],
      order: ["name"],
      include: [
        {
          model: server.Exercise,
          attributes: ["id", "name", "description", "media"],
          include: [
            {
              model: server.Parameter,
              attributes: ["id", "name", "measureRequired"],
              through: {
                attributes: ["id", "required"]
              },
              include: [
                {
                  model: server.Measure,
                  through: {
                    attributes: []
                  },
                  attributes: ["id", "name"]
                }
              ]
            }
          ]
        }
      ]
    })) as ISportInstanceWithExercises[]).map(
      (sportInstance): ISportMeta => ({
        ...sportInstance.toJSON(),
        exercises: sportInstance.exercises.map(
          (exercise): IExerciseMeta => ({
            ...exercise.toJSON(),
            parameters: exercise.parameters.map(
              (parameter): IParameterMeta => {
                const {
                  // @ts-ignore
                  exercise_parameter_relation,
                  ...paramData
                } = parameter.toJSON();
                // console.log({ paramData });
                // console.log({ exercise_parameter_relation });
                const obj = {
                  ...paramData,
                  required: exercise_parameter_relation.required,
                  measures: parameter.measures.map(
                    (measure): IMeasureMeta => ({
                      ...measure.toJSON()
                    })
                  )
                };
                // console.log({ obj });
                return obj;
              }
            )
          })
        )
      })
    );
  }

  public static async getList(
    userId,
    filterParams: ITrainingFilters
  ): Promise<ITraining[]> {
    const result = await server.Training.findAll({
      where: {
        [Op.or]: [{ sportsman: userId }, { trainer: userId }]
        // date: {
        //   [Op.gte]: new Date()
        // }
      },
      attributes: [
        "id",
        "createdAt",
        "updatedAt",
        "date",
        "place",
        "name",
        "comment"
      ],
      include: [
        {
          model: server.User,
          as: "trainerPerson",
          attributes: [
            "id",
            "firstName",
            "lastName",
            "phone",
            "email",
            "avatar",
            "socials"
          ]
        },
        {
          model: server.User,
          as: "sportsmanPerson",
          attributes: [
            "id",
            "firstName",
            "lastName",
            "phone",
            "email",
            "avatar",
            "socials"
          ]
        }
      ],
      order: ["date"]
    });
    return result;
  }

  public static async create(training: INewTraining): Promise<ITrainingItem> {
    const { tasks, ...trainingRest } = training;

    const transaction = await server.dbConnect.transaction();

    try {
      const trainingInstance = await server.Training.create(
        {
          ...trainingRest
        },
        { transaction }
      );

      await TaskMethods.create(
        tasks.map(task => ({ ...task, training: trainingInstance.id })),
        transaction
      );

      await transaction.commit();

      return this.getItem(trainingInstance.id, trainingInstance.sportsman);
    } catch (error) {
      await transaction.rollback();
      if (error instanceof SystemError) {
        throw error;
      }
      throw error;
    }
  }

  public static async getItem(
    trainingId: string,
    userId: string
  ): Promise<ITrainingItem> {
    const result: ITrainingItem = (await server.Training.findOne({
      where: {
        id: trainingId,
        [Op.or]: [
          {
            trainer: userId
          },
          {
            sportsman: userId
          }
        ]
      },
      attributes: ["id", "createdAt", "date", "place", "name", "comment"],
      include: [
        {
          model: server.Task,
          attributes: ["id", "comment"],
          include: [
            {
              model: server.Exercise,
              attributes: ["id", "name"],
              as: "exerciseItem"
            },
            {
              model: server.Set,
              attributes: ["id", "order"],
              include: [
                {
                  model: server.SetPerameterRelation,
                  attributes: ["id", "plan", "fact"],
                  as: "setParameterRelations",
                  include: [
                    {
                      model: server.Parameter,
                      attributes: ["id", "name", "description"],
                      as: "parameterItem"
                    },
                    {
                      model: server.Measure,
                      attributes: ["id", "name", "description"],
                      as: "measureItem"
                    }
                  ]
                }
              ]
            }
          ]
        }
      ]
      // TODO: сортировка по set.order
      // order: ["training->ticket->set.order"],
    })) as any;
    if (!result) {
      throw new SystemError(ErrorMessages.TrainingNotFound);
    }
    return result;
  }

  public static async complete(
    trainingId: string,
    userId: string,
    completedSetParameterRelations: ICompletedSetParameterRelation[]
  ): Promise<ITrainingItem> {
    const transaction = await server.dbConnect.transaction();
    try {
      const Training = await this.getItem(trainingId, userId);
      const setParameterRelationsInTraining: string[] = [];

      Training.tasks.forEach(({ sets }) => {
        sets.forEach(({ setParameterRelations }) => {
          setParameterRelationsInTraining.push(
            ...setParameterRelations.map(({ id }) => id)
          );
        });
      });

      completedSetParameterRelations.forEach(({ setParameterRelationId }) => {
        if (!setParameterRelationsInTraining.includes(setParameterRelationId)) {
          throw new SystemError(
            ErrorMessages.TrainingNotIncludesThisTask(setParameterRelationId)
          );
        }
      });

      await Promise.all(
        completedSetParameterRelations.map(
          ({ fact, setParameterRelationId: id }) =>
            server.SetPerameterRelation.update(
              { fact },
              { where: { id }, transaction }
            )
        )
      );

      await transaction.commit();
      return this.getItem(trainingId, userId);
    } catch (error) {
      await transaction.rollback();
      if (error instanceof SystemError) {
        throw error;
      }
      throw new SystemError(ErrorMessages.CantCompleteTheseTasks);
    }
  }
}
export default Methods;
