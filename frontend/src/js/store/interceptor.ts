import axios, { AxiosResponse } from 'axios'
import { MAIN_URL } from 'Constants';
import Cookies from 'universal-cookie';
import { store } from '../store';

const cookies = new Cookies();

const instance = axios.create({
    baseURL: MAIN_URL,
});



instance.interceptors.request.use(
  async config => {
    if (Number(cookies.get('expiresIn')) * 1000 < Date.now()) {
      console.log(
        'время действия токена вышло',
        Number(cookies.get('expiresIn')) * 1000,
        Date.now()
      );
      await axios({
        url: `${MAIN_URL}auth/refresh`,
        method: 'post',
        // headers: {
        //   authorization: `Bearer ${cookies.get('refreshToken')}`,
        // }
        data: {
          token: cookies.get('token'),
          refreshToken: cookies.get('refreshToken')
        }
      })
        .then((response: AxiosResponse) => {
          console.log(response)
          cookies.set('token', response.data.token)
          cookies.set('refreshToken', response.data.refreshToken)
          cookies.set('expiresIn', response.data.expiresIn)
          config.headers.Authorization = `Bearer ${response.data.token}`
        })
        .catch((error: Error) => {
          console.log(error)
        })
    } else {
      console.log('токен еще действует');
      const token = store.getState().auth.token; // cookies.get('token');
      config.headers.Authorization = `Bearer ${cookies.get('token') || token}`
    }
    return config
  },
  err => {
    return Promise.reject(err)
  }
);
// const instanceExcel = axios.create({
//   baseURL: encodeURI(cookies.get('urlCore'))
// });

// instanceExcel.interceptors.request.use(config => {
//   config.headers.Authorization = `Bearer ${cookies.get('orgToken')}`;
//   return config
// },
//   err => {
//     return Promise.reject(err)
//   }
// );

export default instance

