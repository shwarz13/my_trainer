import { IDecoratedRequest } from "../../../interfaces";
import trycatcher from "../../../utils/trycatcher";
import Methods from "../";
import {
  IAuthPayload,
  IRefreshPayload,
  ILoginResponse,
  IResetPasswordCredentials,
  IResetPasswordPayload,
  IAuthCredentials
} from "../interfaces";
import { routeName } from "../constants";
import { ICreateUserPayload } from "../../User/interfaces";

const AuthCtrl = {
  login: trycatcher(
    async (
      req: IDecoratedRequest<IAuthPayload>,
      h
    ): Promise<ILoginResponse> => {
      const { login, password } = req.payload;
      const tokens = await Methods.login(login, password);

      return tokens;
    },
    { isRequest: true, logMessage: `${routeName} login` }
  ),
  refresh: trycatcher(
    async (
      req: IDecoratedRequest<IRefreshPayload>,
      h
    ): Promise<ILoginResponse> => {
      const { token, refreshToken } = req.payload;
      const tokens = await Methods.refresh(token, refreshToken);

      return tokens;
    },
    { isRequest: true, logMessage: `${routeName} refresh` }
  ),
  registration: trycatcher(
    async (req: IDecoratedRequest<ICreateUserPayload>, h): Promise<boolean> => {
      const userData = req.payload;
      const result = await Methods.registration(userData);

      return result;
    },
    { isRequest: true, logMessage: `${routeName} registration` }
  ),
  resetPassword: trycatcher(
    async (
      req: IDecoratedRequest<IResetPasswordPayload, {}, {}, IAuthCredentials>,
      h
    ): Promise<boolean> => {
      const { oldPassword, newPassword } = req.payload;
      const { id: userId } = req.auth.credentials;
      const result = await Methods.resetPassword(
        userId,
        oldPassword,
        newPassword
      );

      return result;
    },
    { isRequest: true, logMessage: `${routeName} resetPassword` }
  ),
  resetForgottenPassword: trycatcher(
    async (
      req: IDecoratedRequest<{}, {}, { login: string }>,
      h
    ): Promise<boolean> => {
      const { login } = req.params;
      const result = await Methods.resetForgottenPassword(login);

      return result;
    },
    { isRequest: true, logMessage: `${routeName} resetForgottenPassword` }
  ),
  resetForgottenPasswordComplete: trycatcher(
    async (
      req: IDecoratedRequest<
        { newPassword: string },
        {},
        {},
        IResetPasswordCredentials
      >,
      h
    ): Promise<boolean> => {
      const { newPassword } = req.payload;
      const { id, resetToken } = req.auth.credentials;
      const result = await Methods.resetForgottenPasswordComplete(
        id,
        resetToken,
        newPassword
      );

      return result;
    },
    {
      isRequest: true,
      logMessage: `${routeName} resetForgottenPasswordComplete`
    }
  )
};

export default AuthCtrl;
