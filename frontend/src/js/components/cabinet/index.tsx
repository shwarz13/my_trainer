import React, { Component, useEffect, useState } from 'react';
import { hot } from 'react-hot-loader';
import { connect } from 'react-redux';
import { Container } from '@material-ui/core';
import MobileMenu from './menu';
import Header from './header';
import Content from './content';
import FitnessCenterIcon from '@material-ui/icons/FitnessCenter';
import PersonIcon from '@material-ui/icons/Person';
import MenuBookIcon from '@material-ui/icons/MenuBook';
import SystemUpdateIcon from '@material-ui/icons/SystemUpdate';
import loadable from 'loadable-components';
import { getUserInfo } from 'Store/user_info/actions';
import { IHeaderActions, IHeaderState, ISetLeftBtn } from 'Store/header/i';
import { IUserInfoActions } from 'Store/user_info/i';
import Spinner from 'Components/baseComponents/SpinnerLoader';
import { IMainInterfaceActions, IMainInterfaceState } from 'Store/interface/i';
import { makeStyles } from '@material-ui/core/styles';



const path = require('Constants').initialPath;

type IProps = {
	location?: any;
	loaded?: number;
} & IUserInfoActions & IHeaderActions & IMainInterfaceState;

const useStyles = makeStyles(theme => ({
	root: {
		padding: '0px'
	}
}));

const Cabinet = (props: IProps) => {

	const [currentTitle, setCurrentTitle] = useState('');

	const routes = [
		{
			title: 'Тренировка',
			path: `${path}lk/training`,
			image: <FitnessCenterIcon />,
			component: loadable(() => import('./content/trainings'))
		},
		{
			title: 'Новости',
			path: `${path}lk/news`,
			image: <SystemUpdateIcon />,
			component: loadable(() => import('./content/news'))
		},
		{
			title: 'Упражнения',
			path: `${path}lk/exercises`,
			image: <MenuBookIcon />,
			component: loadable(() => import('./content/exersices'))
		},
		{
			title: 'Профиль',
			path: `${path}lk/profile`,
			image: <PersonIcon />,
			notifications: 6,
			component: loadable(() => import('./content/profile'))
		}
	];

	const classes = useStyles();

	useEffect(() => {

		const menuItem = routes.filter(el => {
			const regex = new RegExp(`${el.path.substring(0, el.path.length - 3)}\\w{1,3}\\/*`);
			return regex.test(props.location.pathname);
		});
		setCurrentTitle(menuItem[0] ? menuItem[0].title: '');

	}, [props.location.pathname]);

	useEffect(() => {
		props.getUserInfo({});
	}, []);

	return (
		<Container component="main" maxWidth="xs" className={classes.root}>
			<Header title={currentTitle}/>
			{
				props.loaded === 2  && !props.isSpinnerShow ?
					<Content routes={[...routes]}/> :
					<Spinner/>
			}
			<MobileMenu currentPath={props.location.pathname} routes={[...routes]} setTitle={setCurrentTitle}/>
		</Container>
	);
};

const mapStateToProps = (state) => {
	return  {
		loaded: state.loaded.GET_USER_INFO,
		userInfo: state.userInfo.userInfo.data,
		isSpinnerShow: state.mainInterface.isSpinnerShow
	}
};

const mapDispatchToProps = { getUserInfo };

export default hot(module)(
	connect(
		mapStateToProps,
		mapDispatchToProps
	)(Cabinet)
);
