import bcrypt from "bcryptjs";

export default class AuthToolkit {
  public static getHash(value) {
    const salt = bcrypt.genSaltSync();
    return bcrypt.hashSync(value, salt);
  }

  public static isValidPassword(hash, password) {
    return bcrypt.compare(password, hash);
  }
}
