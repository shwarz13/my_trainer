import { IBaseInstanceFields } from "../../../interfaces";
import Sequelize from "sequelize";
import { IExerciseMeta } from "../../Exercise/interfaces";

/* *
 *   Sport
 * */

export interface ISportBase {
  name: string;
  description?: string | null;
}

export interface ISport extends ISportBase, IBaseInstanceFields {}

export type ISportInstance = Sequelize.Instance<ISport> & ISport;

export interface ISportMeta extends ISportBase {
  id: string;
  exercises: IExerciseMeta[];
}
