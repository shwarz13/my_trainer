import { HTTP_ERROR_CODES } from "../../../constants";
const errors = {
  userNotFound: {
    code: HTTP_ERROR_CODES.NOT_FOUND,
    message: "Пользователь не найден. Неверный логин или пароль"
  },
  badRefreshToken: {
    code: HTTP_ERROR_CODES.BAD_REQUEST,
    message: "Некорретный рефреш токен. Требуется повторная авторизация"
  },
  passwordResetError: {
    code: HTTP_ERROR_CODES.BAD_REQUEST,
    message:
      "Не удалось обновить пароль. Без паники, просто обратитесь в техническую поддержку нашего сервиса"
  },
  compareOldPasswordAndHashError: {
    code: HTTP_ERROR_CODES.BAD_REQUEST,
    message: "Неверный старый пароль. Повторите попытку снова."
  }
};
export default errors;
