import { AnyAction } from 'redux';
import { IRegisterState, ActionTypes as AT } from './i';
import { RequestState } from 'Constants';
import Cookies from 'universal-cookie';


const initialState: IRegisterState = {
  clientRegister: false,
  statusCodeFail: false,
  statusCodeSuccess: false
};
// const cookies = new Cookies();
//
export default (state = initialState, action: AnyAction) => {
  switch (action.type) {
    case AT.CLIENT_REGISTER + RequestState.SUCCESS: {
      return {
        ...state,
        clientRegister: true
      };
    }
    case AT.GET_CODE + RequestState.SUCCESS: {
      return {
        ...state,
        statusCodeFail: false,
        statusCodeSuccess: true
      };
    }
    case AT.GET_CODE + RequestState.FAIL: {
      return {
        ...state,
        statusCodeSuccess: false,
        statusCodeFail: true
      };
    }
    default: {
      return state;
    }
  }
}
