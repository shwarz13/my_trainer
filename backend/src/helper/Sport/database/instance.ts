import Sequelize from "sequelize";
import { ISportInstance, ISport, ISportBase } from "../interfaces";
import SportSchema from "./models";
import { BASE_INSTANCE_PARAMS } from "../../../database/constants";

export default (sequelize: Sequelize.Sequelize) => {
  const instance = sequelize.define<ISportInstance, ISport, ISportBase>(
    "sport",
    SportSchema,
    {
      ...BASE_INSTANCE_PARAMS
    }
  );

  return instance;
};
