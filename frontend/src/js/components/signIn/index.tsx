import React, { Component, useState } from 'react';
import { hot } from 'react-hot-loader';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';

import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
// import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { clientLogin } from 'Store/auth/actions';

function Copyright() {
	return (
		<Typography variant="body2" color="textSecondary" align="center">
			{'Copyright © '}
			<Link color="inherit" href="#">
				Your Train
			</Link>{' '}
			{new Date().getFullYear()}
			{'.'}
		</Typography>
	);
}

const path = require('Constants').initialPath;

const useStyles = makeStyles(theme => ({
	paper: {
		marginTop: theme.spacing(4),
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
	},
	form: {
		width: '100%', // Fix IE 11 issue.
		marginTop: theme.spacing(1),
	},
	submit: {
		margin: theme.spacing(3, 0, 2),
	},
}));


const SignIn = (props) => {
	const classes = useStyles();
	const [login, setLogin] = useState('');
	const [password, setPassword] = useState('');

	const handleLogin = async e => {
		e.preventDefault();
		await props.clientLogin({ login, password });
		props.history.push("/lk");
	};

	return (
		<Container component="main" maxWidth="xs" className={'absolute_centre bg_light'}>
			<CssBaseline />
			<div className={classes.paper}>
				<Typography component="h1" variant="h5">
					Добро пожаловать!
				</Typography>
				<form className={classes.form} onSubmit={handleLogin} noValidate>
					<TextField
						variant="outlined"
						margin="normal"
						required
						fullWidth
						id="email"
						label="Введите адрес электронной почты"
						name="email"
						autoComplete="email"
						autoFocus
						value={login}
						onChange={e => setLogin(e.target.value)}
					/>
					<TextField
						variant="outlined"
						margin="normal"
						required
						fullWidth
						name="password"
						label="Введите пароль"
						type="password"
						id="password"
						autoComplete="current-password"
						value={password}
						onChange={e => setPassword(e.target.value)}
					/>
					<FormControlLabel
						control={<Checkbox value="remember" color="primary" />}
						label="Запомнить меня"
					/>
					<Button
						type="submit"
						fullWidth
						variant="contained"
						color="primary"
						className={classes.submit}
					>
						Войти в систему
					</Button>
					<Grid container>
						<Grid item xs>
							<NavLink to={`${path}sign_up`}>Забыли пароль?</NavLink>
						</Grid>
						<Grid item>
							<NavLink to={`${path}sign_up`}>Регистрация</NavLink>
						</Grid>
					</Grid>
				</form>
			</div>
			<Box mt={5} mb={1}>
				<Copyright />
			</Box>
		</Container>
	);
};

const mapStateToProps = (state) => {
	return  {
		loggingIn: state.auth.loggingIn,
		status: state.loaded.CLIENT_LOGIN
	}
};

const mapDispatchToProps = { clientLogin };

export default hot(module)(
	connect(
		mapStateToProps,
		mapDispatchToProps
	)(SignIn)
);
