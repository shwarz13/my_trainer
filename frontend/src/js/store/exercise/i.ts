import { IAction } from '../i';

// state
export interface IExercisesState {
    exercises?: any[];
    images?: any[];
    currentExercise?: any;
}

// actions
export interface IExercisesActions {
    getExercises?: IGetExercises;
    getImages?: IGetImages;
    getOneExercise?: IGetOneExercise;
    createExercise?: ICreateExercise;
    updateExercise?: IUpdateExercise;
}
export enum ActionTypes {
    GET_EXERCISES = 'GET_EXERCISES',
    GET_ONE_EXERCISE = 'GET_ONE_EXERCISE',
    GET_IMAGES = 'GET_IMAGES',
    CREATE_EXERCISE = 'CREATE_EXERCISE',
    UPDATE_EXERCISE = 'UPDATE_EXERCISE',
}
// other
export type IGetExercises = IAction<{}, any>;
export type IGetImages = IAction<{}, any>;
export type ICreateExercise = IAction<{
    name:string,
    type?:string,
    inn?:string,
    kpp?:string,
    email?:string,
    phone?:string,
    website?:string
}, any>;
export type IUpdateExercise = IAction<{
    id: string,
    name?:string,
    type?:string,
    inn?:string,
    kpp?:string,
    email?:string,
    phone?:string,
    website?:string
}, any>;
export type IGetOneExercise = IAction<{id:string}, any>;
