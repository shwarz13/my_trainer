const path = require('path');
const root = path.resolve(__dirname, '../../');

module.exports = {
    parser: 'postcss-scss',    
	plugins: {
		'postcss-import': {
			resolve: (id, basedir) => {
				const alias = [
					{ name: '~css', toPath: 'src/css' },
					{ name: '~img', toPath: 'src/img' },
					{ name: 'node_modules', toPath: 'node_modules' }
				];

				for (let { name, toPath } of alias)
					if (id.substr(0, name.length) === name)
						return `${root}/${toPath}/${id.substr(name.length + 1)}`;

				return path.resolve(basedir, id);
			}
		}
	}
};
