import Sequelize from "sequelize";
import {
  IParameterMeasureRelationInstance,
  IParameterMeasureRelation,
  IParameterMeasureRelationBase
} from "../interfaces";
import ParameterMeasureRelationSchema from "./models";
import { BASE_INSTANCE_PARAMS } from "../../../database/constants";

export default (sequelize: Sequelize.Sequelize) => {
  const instance = sequelize.define<
    IParameterMeasureRelationInstance,
    IParameterMeasureRelation,
    IParameterMeasureRelationBase
  >("parameter_measure_relation", ParameterMeasureRelationSchema, {
    ...BASE_INSTANCE_PARAMS
  });

  return instance;
};
